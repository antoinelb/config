-- vim: set foldmethod=marker foldlevel=0 nomodeline:

-- settings {{{

vim.g.mapleader = ","
vim.g.maplocalleader = ","

-- options {{{

local opts = { noremap = true, silent = true }

vim.opt.autochdir = true -- change dir to current file dir
vim.opt.autoindent = true -- new line indent is same as previous
vim.opt.autoread = true -- automatically read file that has been changed outside vim
vim.opt.backupcopy = "yes" -- disables safe write to enable hot module replacement
vim.opt.breakindent = true -- line wrap keeps indentation
vim.opt.clipboard = "unnamedplus" -- have clipboard be system clipboard
vim.opt.cmdheight = 0 -- height of the command section
vim.opt.colorcolumn = "80" -- put colored column
vim.opt.completeopt = "menuone,noselect" -- options for complete menus
vim.opt.conceallevel = 0 -- amount visual should reflect what it is
vim.opt.cursorline = true -- find current line quickly
vim.opt.encoding = "utf-8" -- displayed encoding
vim.opt.expandtab = true -- change tab to spaces
vim.opt.filetype = "on" -- detect filetypes
vim.opt.foldexpr = "nvim_treesitter#foldexpr()" -- method to use for folding
vim.opt.foldlevel = 0 -- initial fold level
vim.opt.foldmethod = "expr" -- method to use for folding
vim.opt.formatoptions = "jcroqlnt" -- various options for formatting
vim.opt.gdefault = true -- makes all substitute be global
vim.opt.hidden = true -- allow buffer switching with unsave files
vim.opt.hlsearch = false -- disable highlight search
vim.opt.laststatus = 1 -- adds status if at least 2 files
vim.opt.lazyredraw = true -- redraw only when necessary
vim.opt.nrformats = "alpha" -- characters are considered for incrementing
vim.opt.number = true -- show line numbers
vim.opt.previewheight = 5 -- Height of the preview window
vim.opt.relativenumber = true -- line number is relative to cursor
vim.opt.scrolloff = 10 -- minimum number of lines to keep below and above cursor
vim.opt.shiftwidth = 2 -- tab width
vim.opt.showcmd = false -- don't show last command as last line of screen
vim.opt.showmatch = true -- show matching bracket
vim.opt.showmode = false -- don't show the mode as last line
vim.opt.signcolumn = "yes" -- if sign column should always be present
vim.opt.softtabstop = 2 -- tab width
vim.opt.splitbelow = true -- vertical split is always below
vim.opt.swapfile = false -- disable swap file
vim.opt.tabstop = 2 -- tab width
vim.opt.textwidth = 0 -- disable limit of chars to paste
vim.opt.timeoutlen = 300 -- time to wait for mapped sequence to complete
vim.opt.undofile = true -- save undos once file is closed
vim.opt.undolevels = 1000 -- number of undos to save
vim.opt.undoreload = 10000 -- number of lines to save for undo
vim.opt.updatetime = 300 -- update time delay
vim.opt.wildmode = "longest:full,full" -- mode for finding files
vim.opt.wrapmargin = 0 -- prevent line breaks

vim.opt.iskeyword:remove("-,:,#")
vim.opt.listchars:append("space:·")
vim.opt.listchars:append("eol:↴")

vim.opt.wildignore:append("*.pyc")
vim.opt.wildignore:append("**/__pycache__/*")
vim.opt.wildignore:append("*.zip")
vim.opt.wildignore:append("*.tar")

vim.opt.dictionary:append("/usr/share/dict/words")
vim.opt.dictionary:append("/usr/share/dict/french")

vim.g.do_filetype_lua = true

-- }}}

-- autocmds {{{

-- disable comments on new lines
vim.cmd([[
  augroup NewLineComment
    autocmd!
    autocmd BufWinEnter,BufNewFile,BufRead * setlocal formatoptions-=cro
  augroup end
]])

-- show yanked text
vim.cmd([[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]])

-- go to last location when opening buffer
vim.api.nvim_create_autocmd("BufReadPre", {
	pattern = "*",
	callback = function()
		local mark = vim.api.nvim_buf_get_mark(0, '"')
		local lcount = vim.api.nvim_buf_line_count(0)
		if mark[1] > 0 and mark[1] <= lcount then
			pcall(vim.api.nvim_win_set_cursor, 0, mark)
		end
	end,
})

-- windows to close
vim.api.nvim_create_autocmd("FileType", {
	pattern = {
		"OverseerForm",
		"OverseerList",
		"floggraph",
		"fugitive",
		"git",
		"help",
		"lspinfo",
		"man",
		"neotest-output",
		"neotest-summary",
		"qf",
		"query",
		"spectre_panel",
		"startuptime",
		"toggleterm",
		"tsplayground",
		"vim",
	},
	callback = function(event)
		vim.bo[event.buf].buflisted = false
		vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
	end,
})

-- }}}

-- icons {{{
local icons = {
	kind = {
		Array = "",
		Boolean = "",
		Class = "",
		Color = "",
		Constant = "",
		Constructor = "",
		Enum = "",
		EnumMember = "",
		Event = "",
		Field = "",
		File = "",
		Folder = "",
		Function = "",
		Interface = "",
		Key = "",
		Keyword = "",
		Method = "",
		Module = "",
		Namespace = "",
		Null = "ﳠ",
		Number = "",
		Object = "",
		Operator = "",
		Package = "",
		Property = "",
		Reference = "",
		Snippet = "",
		String = "",
		Struct = "",
		Text = "",
		TypeParameter = "",
		Unit = "",
		Value = "",
		Variable = "",
	},
	git = {
		LineAdded = "",
		LineModified = "",
		LineRemoved = "",
		FileDeleted = "",
		FileIgnored = "◌",
		FileRenamed = "",
		FileStaged = "S",
		FileUnmerged = "",
		FileUnstaged = "",
		FileUntracked = "U",
		Diff = "",
		Repo = "",
		Octoface = "",
		Branch = "",
	},
	ui = {
		ArrowCircleDown = "",
		ArrowCircleLeft = "",
		ArrowCircleRight = "",
		ArrowCircleUp = "",
		BoldArrowDown = "",
		BoldArrowLeft = "",
		BoldArrowRight = "",
		BoldArrowUp = "",
		BoldClose = "",
		BoldDividerLeft = "",
		BoldDividerRight = "",
		BoldLineLeft = "▎",
		BookMark = "",
		BoxChecked = "",
		Bug = "",
		Stacks = "",
		Scopes = "",
		Watches = "",
		DebugConsole = "",
		Calendar = "",
		Check = "",
		ChevronRight = ">",
		ChevronShortDown = "",
		ChevronShortLeft = "",
		ChevronShortRight = "",
		ChevronShortUp = "",
		Circle = "",
		Close = "",
		CloudDownload = "",
		Code = "",
		Comment = "",
		Dashboard = "",
		DividerLeft = "",
		DividerRight = "",
		DoubleChevronRight = "»",
		Ellipsis = "",
		EmptyFolder = "",
		EmptyFolderOpen = "",
		File = "",
		FileSymlink = "",
		Files = "",
		FindFile = "",
		FindText = "",
		Fire = "",
		Folder = "",
		FolderOpen = "",
		FolderSymlink = "",
		Forward = "",
		Gear = "",
		History = "",
		Lightbulb = "",
		LineLeft = "▏",
		LineMiddle = "│",
		List = "",
		Lock = "",
		NewFile = "",
		Note = "",
		Package = "",
		Pencil = "",
		Plus = "",
		Project = "",
		Search = "",
		SignIn = "",
		SignOut = "",
		Tab = "",
		Table = "",
		Target = "",
		Telescope = "",
		Text = "",
		Tree = "",
		Triangle = "契",
		TriangleShortArrowDown = "",
		TriangleShortArrowLeft = "",
		TriangleShortArrowRight = "",
		TriangleShortArrowUp = "",
	},
	diagnostics = {
		BoldError = "",
		Error = "",
		BoldWarning = "",
		Warning = "",
		BoldInformation = "",
		Information = "",
		BoldQuestion = "",
		Question = "",
		BoldHint = "",
		Hint = "",
		Debug = "",
		Trace = "✎",
	},
	misc = {
		Robot = "ﮧ",
		Squirrel = "",
		Tag = "",
		Watch = "",
		Smiley = "",
		Package = "",
		CircuitBoard = "",
	},
}
-- }}}

-- }}}

-- plugins {{{

-- plugins {{{

-- utils {{{
local lsp_utils = {}

local function _list_registered_providers_names(ft)
	local s = require("null-ls.sources")
	local available_sources = s.get_available(ft)
	local registered = {}
	for _, source in ipairs(available_sources) do
		for method in pairs(source.methods) do
			registered[method] = registered[method] or {}
			table.insert(registered[method], source.name)
		end
	end
	return registered
end

function lsp_utils.list_formatters(ft)
	local providers = _list_registered_providers_names(ft)
	return providers[require("null-ls").methods.FORMATTING] or {}
end

function lsp_utils.list_linters(ft)
	local providers = _list_registered_providers_names(ft)
	return providers[require("null-ls").methods.DIAGNOSTICS] or {}
end

function lsp_utils.list_completions(ft)
	local providers = _list_registered_providers_names(ft)
	return providers[require("null-ls").methods.COMPLETION] or {}
end

function lsp_utils.list_code_actions(ft)
	local providers = _list_registered_providers_names(ft)
	return providers[require("null-ls").methods.CODE_ACTION] or {}
end

function lsp_utils.list_hovers(ft)
	local providers = _list_registered_providers_names(ft)
	return providers[require("null-ls").methods.HOVER] or {}
end

-- }}}

local plugins = {

	-- utilities {{{
	"nvim-lua/plenary.nvim",
	"MunifTanjim/nui.nvim",
	{ "lewis6991/impatient.nvim", lazy = false },
	{ "kshenoy/vim-signature", lazy = false },
	{ "christoomey/vim-sort-motion", lazy = false },
	{ "tpope/vim-surround", lazy = false },
	{ "tpope/vim-repeat", lazy = false },
	{
		"nvim-tree/nvim-web-devicons",
		config = { default = true },
	},
	{
		"vim-scripts/camelcasemotion",
		lazy = false,
		config = function()
			vim.cmd([[
        map w <Plug>CamelCaseMotion_w
        map b <Plug>CamelCaseMotion_b
        map e <Plug>CamelCaseMotion_e
        sunmap w
        sunmap b
        sunmap e
      ]])
		end,
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		event = "BufReadPre",
		config = true,
	},
	{
		"stevearc/dressing.nvim",
		event = "VeryLazy",
		config = true,
	},
	{
		"rcarriga/nvim-notify",
		event = "VeryLazy",
		enabled = true,
		config = { default = true },
	},
	{
		"sindrets/diffview.nvim",
		cmd = {
			"DiffviewOpen",
			"DiffviewClose",
			"DiffviewToggleFiles",
			"DiffviewFocusFiles",
		},
		config = true,
	},
	{
		"TimUntersberger/neogit",
		cmd = "Neogit",
		keys = {
			{ "<leader>gs", "<cmd>Neogit kind=floating<cr>", desc = "Status" },
		},
		config = function()
			require("neogit").setup({
				disable_commit_confirmation = false,
				popup = {
					kind = "replace",
				},
			})
		end,
	},
	{
		"tpope/vim-characterize",
		event = "VeryLazy",
	},
	-- }}}

	-- color scheme {{{
	{
		"shaunsingh/nord.nvim",
		lazy = false,
		config = function()
			vim.g.nord_contrast = true
			vim.g.nord_borders = true
			vim.g.nord_disable_background = true
			vim.g.nord_italic = true
			vim.g.nord_bold = true
			require("nord").set()
		end,
	},
	-- }}}

	-- better comments {{{
	{
		"numToStr/Comment.nvim",
		keys = { "gc", "gcc", "gbc", { "gc", mode = "v" } },
		config = function()
			require("Comment").setup({
				mappings = {
					basic = true,
					extra = true,
				},
			})
		end,
	},
	-- }}}

	-- markdown {{{
	{
		"iamcco/markdown-preview.nvim",
		run = function()
			vim.fn["mkdp#util#install"]()
		end,
		ft = "markdown",
		cmd = { "MarkdownPreview" },
	},
	"plasticboy/vim-markdown",
	-- }}}

	-- telescope {{{
	{
		"nvim-telescope/telescope.nvim",
		cmd = "Telescope",
		dependencies = {
			"stevearc/aerial.nvim",
			"nvim-telescope/telescope-file-browser.nvim",
		},
		keys = {
			{ "<c-f>", "<cmd>Telescope find_files<cr>", opts },
			{ "<c-g>", "<cmd>Telescope git_files<cr>", opts },
			{ "<c-p>", "<cmd>Telescope live_grep<cr>", opts },
			{ "<c-t>", "<cmd>Telescope aerial<cr>", opts },
			{
				"<c-b>",
				"<cmd>lua require('telescope.builtin').buffers({ sort_lastused = true, ignore_current_buffer = true })<cr>",
				opts,
			},
			{ "<c-d>", "<cmd>Telescope file_browser grouped=true<cr>", opts },
		},
		config = function()
			require("telescope").setup({
				extensions = {
					file_browser = {
						hijack_netrw = true,
					},
				},
			})
			require("telescope").load_extension("aerial")
			require("telescope").load_extension("file_browser")
		end,
	},
	{
		"stevearc/aerial.nvim",
		config = function()
			require("aerial").setup({
				backends = { "treesitter", "lsp" },
			})
		end,
	},
	-- }}}

	-- treesitter {{{
	{
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
			-- "yioneko/nvim-yati"
		},
		build = ":TSUpdate",
		event = "BufReadPost",
		config = function()
			local swap_next, swap_prev = (function()
				local swap_objects = {
					p = "@parameter.inner",
					f = "@function.outer",
					c = "@class.outer",
				}

				local n, p = {}, {}
				for key, obj in pairs(swap_objects) do
					n[string.format("<leader>cx%s", key)] = obj
					p[string.format("<leader>cX%s", key)] = obj
				end

				return n, p
			end)()

			require("nvim-treesitter.configs").setup({
				ensure_installed = {
					"css",
					"html",
					"javascript",
					"json",
					"lua",
					"markdown",
					"markdown_inline",
					"python",
					"query",
					"regex",
					"vim",
					"vimdoc",
					"yaml",
				},
				highlight = { enable = true },
				indent = { enable = true },
				incremental_selection = {
					enable = true,
					keymaps = {
						init_selection = "gnn",
						node_incremental = "grn",
						scope_incremental = "grc",
						node_decremental = "grm",
					},
				},
				textobjects = {
					select = {
						enable = true,
						lookahead = true,
						keymaps = {
							["aa"] = "@parameter.outer",
							["ia"] = "@parameter.inner",
							["af"] = "@function.outer",
							["if"] = "@function.inner",
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
						},
					},
					move = {
						enable = true,
						set_jumps = true,
						goto_next_start = {
							["]m"] = "@function.outer",
							["]]"] = "@class.outer",
						},
						goto_next_end = {
							["]M"] = "@function.outer",
							["]["] = "@class.outer",
						},
						goto_previous_end = {
							["[M"] = "@function.outer",
							["[]"] = "@class.outer",
						},
					},
					swap = {
						enable = true,
						swap_next = swap_next,
						swap_previous = swap_prev,
					},
				},
				matchup = {
					enable = true,
				},
			})

			vim.treesitter.query.set(
				"python",
				"folds",
				[[
        [
          (function_definition)
          (class_definition)
          (string)
        ] @fold
      ]]
			)
		end,
	},
	-- }}}

	-- completion {{{
	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-cmdline",
		},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			local neogen = require("neogen")
			local compare = require("cmp.config.compare")

			local source_names = {
				nvim_lsp = "(LSP)",
				luasnip = "(Snippet)",
				buffer = "(Buffer)",
				path = "(Path)",
			}
			local duplicates = {
				buffer = 1,
				path = 1,
				nvim_lsp = 0,
				luasnip = 1,
			}

			local has_words_before = function()
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0
					and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			cmp.setup({
				completion = {
					completeopt = "menu,menuone,noinsert,noselect",
				},
				preselect = cmp.PreselectMode.None,
				sorting = {
					priority_weight = 2,
					compare.score,
					compare.recently_used,
					compare.offset,
					compare.exact,
					compare.kind,
					compare.sort_text,
					compare.length,
					compare.order,
				},
				snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end,
				},
				mapping = cmp.mapping.preset.insert({
					["<c-b>"] = cmp.mapping.scroll_docs(-4),
					["<c-f>"] = cmp.mapping.scroll_docs(4),
					["<c-space>"] = cmp.mapping.complete(),
					["<c-e>"] = cmp.mapping.abort(),
					["<cr>"] = cmp.mapping.confirm({ select = false }),
					["<tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif neogen.jumpable() then
							neogen.jump_next()
						elseif luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						elseif has_words_before() then
							cmp.complete()
						else
							fallback()
						end
					end, { "i", "s" }),
					["<s-tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif neogen.jumpable(true) then
							neogen.jump_prev()
						elseif luasnip.jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
					end, { "i", "s" }),
				}),
				sources = cmp.config.sources({
					{ name = "nvim_lsp_signature_help", group_index = 1 },
					{ name = "nvim_lsp", group_index = 1 },
					{ name = "luasnip", group_index = 1 },
					{ name = "buffer", group_index = 2 },
					{ name = "path", group_index = 2 },
				}),
				formatting = {
					fields = { "kind", "abbr", "menu" },
					format = function(entry, item)
						local max_width = 80
						local duplicates_default = 0
						if max_width ~= 0 and #item.abbr > max_width then
							item.abbr = string.sub(item.abbr, 1, max_width - 1) .. icons.ui.Ellipsis
						end
						item.kind = icons.kind[item.kind]
						item.menu = source_names[entry.source.name]
						item.dup = duplicates[entry.source.name] or duplicates_default
						return item
					end,
				},
			})

			cmp.setup.cmdline({ "/", "?" }, {
				mapping = cmp.mapping.preset.cmdline(),
				sources = {
					{ name = "buffer" },
				},
			})

			cmp.setup.cmdline(":", {
				mapping = cmp.mapping.preset.cmdline(),
				sources = cmp.config.sources({
					{
						name = "path",
						option = {
							trailing_slash = true,
						},
					},
				}, {
					{ name = "cmdline" },
				}),
			})

			local cmp_autopairs = require("nvim-autopairs.completion.cmp")
			cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))
		end,
	},
	{
		"L3MON4D3/LuaSnip",
		dependencies = {
			"rafamadriz/friendly-snippets",
			config = function()
				require("luasnip.loaders.from_vscode").lazy_load()
			end,
		},
		config = {
			history = true,
			delete_check_events = "TextChanged",
		},
	},
	-- }}}

	-- lsp {{{
	{
		"neovim/nvim-lspconfig",
		event = "BufReadPre",
		dependencies = {
			{ "folke/neoconf.nvim", cmd = "Neoconf", config = true },
			{
				"folke/neodev.nvim",
				opts = {
					library = {
						plugins = { "neotest", "nvim-dap-ui" },
						types = true,
					},
				},
			},
			{
				"j-hui/fidget.nvim",
				tag = "legacy",
				config = function()
					require("fidget").setup({
						window = {
							blend = 0,
						},
					})
				end,
			},
			{ "smjonas/inc-rename.nvim", config = true },
			"williamboman/mason.nvim",
			"williamboman/mason-lspconfig.nvim",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-nvim-lsp-signature-help",
			"barreiroleo/ltex-extra.nvim",
		},
		config = function(_) -- {{{
			-- servers {{{
			local servers = {
				pyright = {
					settings = {
						python = {
							analysis = {
								autoImportCompletions = true,
								typeCheckingMode = "off",
								autoSearchPaths = true,
								useLibraryCodeForTypes = true,
								diagnosticMode = "openFilesOnly",
								diagnosticSeverityOverrides = {
									reportMissingImports = "none",
								},
							},
						},
					},
				},
				ruff_lsp = {
					settings = {},
				},
				lua_ls = {
					settings = {
						Lua = {
							workspace = {
								checkThirdParty = false,
							},
							completion = { callSnippet = "Replace" },
							telemetry = { enable = false },
							hint = {
								enable = false,
							},
							diagnostics = {
								globals = { "vim" },
							},
						},
					},
				},
				tsserver = {
					disable_formatting = false,
				},
				emmet_ls = {
					html = {
						options = {
							["bem.enabled"] = true,
						},
					},
				},
				cssls = {},
				stylelint_lsp = {
					settings = {
						stylelintplus = {
							autoFixOnFormat = true,
							autoFixOnSave = true,
							config = {
								plugins = {
									"stylelint-order",
									"stylelint-prettier",
								},
								extends = "stylelint-config-recommended",
								rules = {
									["order/order"] = {
										"custom-properties",
										"declarations",
									},
									["order/properties-alphabetical-order"] = true,
									["no-descending-specificity"] = nil,
									["no-empty-source"] = nil,
									["prettier/prettier"] = true,
								},
							},
						},
					},
				},
				html = {},
				texlab = {
					settings = {
						texlab = {
							diagnostics = {
								ignoredPatterns = {
									"Overfull",
									"Underfull",
									"Package balance",
									"Unused global",
									"Package fancyhdr Warning",
									"There's no line here to end",
									"No hyphenation patterns",
								},
							},
						},
					},
				},
				ltex = {
					on_attach = function(_, _)
						-- your other on_attach functions.
						require("ltex_extra").setup({
							load_langs = { "en-CA", "fr" },
							init_check = true,
							path = vim.fn.expand("~") .. "/.local/share/ltex",
						})
					end,
					settings = {
						ltex = {
							enabled = { "latex" },
							language = "auto",
						},
					},
				},
			}
			-- }}}

			local function lsp_attach(on_attach) -- {{{
				vim.api.nvim_create_autocmd("LspAttach", {
					callback = function(args)
						local buffer = args.buf
						local client = vim.lsp.get_client_by_id(args.data.client_id)
						on_attach(client, buffer)
					end,
				})
			end -- }}}

			local function lsp_capabilities() -- {{{
				local capabilities = vim.lsp.protocol.make_client_capabilities()
				capabilities.textDocument.foldingRange = {
					dynamicRegistration = false,
					lineFoldingOnly = true,
				}
				return require("cmp_nvim_lsp").default_capabilities(capabilities)
			end -- }}}

			local function format(client, buffer) -- {{{
				if client.supports_method("textDocument/formatting") then
					vim.api.nvim_create_autocmd("BufWritePre", {
						group = vim.api.nvim_create_augroup("LspFormat." .. buffer, {}),
						buffer = buffer,
						callback = function()
							local buf = vim.api.nvim_get_current_buf()
							local ft = vim.bo[buf].filetype
							local have_nls = #require("null-ls.sources").get_available(ft, "NULL_LS_FORMATTING") > 0

							vim.lsp.buf.format({
								bufnr = buffer,
								filter = function(client_)
									if have_nls then
										return client_.name == "null-ls"
									end
									return client_.name ~= "null-ls"
								end,
								timeout_ms = 2000,
							})
						end,
					})
				end
			end -- }}}

			local function keymappings(_, buffer) -- {{{
				vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = buffer })
				vim.keymap.set("n", "<c-h>", "<cmd>lua vim.diagnostic.goto_prev()<cr>", opts)
				vim.keymap.set("n", "<c-l>", "<cmd>lua vim.diagnostic.goto_next()<cr>", opts)
				vim.keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<cr>", opts)
				vim.keymap.set("n", "gr", "<cmd>Telescope lsp_references<cr>", opts)
				vim.keymap.set("n", "gD", "<cmd>Telescope lsp_declarations<cr>", opts)
				vim.keymap.set("n", "gI", "<cmd>Telescope lsp_implementations<cr>", opts)
				vim.keymap.set("n", "gb", "<cmd>Telescope lsp_type_definitions<cr>", opts)
				vim.keymap.set("n", "K", "<cmd>lua vim.lsp.buf.hover()<cr>", opts)
				vim.keymap.set("n", "gK", "<cmd>lua vim.lsp.buf.signature_help()<cr>", opts)
				vim.keymap.set("n", "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
				vim.api.nvim_set_keymap(
					"i",
					"<tab>",
					[[pumvisible() ? "\<c-n>" : "\<tab>"]],
					{ noremap = true, expr = true }
				)
				vim.api.nvim_set_keymap(
					"i",
					"<s-tab>",
					[[pumvisible() ? "\<c-p>" : "\<s-tab>"]],
					{ noremap = true, expr = true }
				)
				vim.keymap.set("n", "<leader>=", "<cmd>lua vim.lsp.buf.format()<cr>", opts)
				vim.keymap.set(
					"n",
					"<leader>cs",
					"<cmd>lua require('telescope.builtin').lsp_document_symbols()<cr>",
					opts
				)
				vim.keymap.set(
					"n",
					"<leader>cS",
					"<cmd>lua require('telescope.builtin').lsp_workspace_symbols()<cr>",
					opts
				)
			end -- }}}

			lsp_attach(function(client, buffer)
				format(client, buffer)
				keymappings(client, buffer)
			end)

			require("mason-lspconfig").setup({ ensure_installed = vim.tbl_keys(servers) })
			require("mason-lspconfig").setup_handlers({
				function(server)
					local opts_ = servers[server] or {}
					opts_.capabilities = lsp_capabilities()
					require("lspconfig")[server].setup(opts_)
				end,
			})
		end, -- }}}
	},
	{
		"williamboman/mason.nvim",
		cmd = "Mason",
		keys = {
			{ "<leader>cm", "<cmd>Mason<cr>" },
		},
		ensure_installed = {
			"black",
			"isort",
			"mypy",
			"stylua",
			"latexindent",
		},
		config = function(plugin) -- {{{
			require("mason").setup()
			local mr = require("mason-registry")
			for _, tool in ipairs(plugin.ensure_installed) do
				local p = mr.get_package(tool)
				if not p:is_installed() then
					p:install()
				end
			end
		end, -- }}}
	},
	{
		"jose-elias-alvarez/null-ls.nvim",
		event = "BufReadPre",
		dependencies = { "mason.nvim" },
		config = function() -- {{{
			local nls = require("null-ls")
			nls.setup({
				sources = {
					nls.builtins.formatting.prettierd.with({
						filetypes = { "html", "json", "yaml", "javascript" },
					}),
					nls.builtins.formatting.stylua,
					nls.builtins.formatting.isort,
					nls.builtins.formatting.black.with({
						args = { "--stdin-filename", "$FILENAME", "--quiet", "--line-length", "79", "-" },
					}),
					nls.builtins.formatting.latexindent,
					nls.builtins.diagnostics.mypy,
					nls.builtins.diagnostics.eslint,
					nls.builtins.diagnostics.stylelint,
				},
			})
		end, -- }}}
	},
	-- }}}

	-- status line {{{
	{
		"nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		config = function()
			local components = {
				-- {{{
				spaces = {
					function()
						local shiftwidth = vim.api.nvim_buf_get_option(0, "shiftwidth")
						return icons.ui.Tab .. " " .. shiftwidth
					end,
					padding = 1,
				},
				git_repo = {
					function()
						if
							#vim.api.nvim_list_tabpages() > 1
							and vim.fn.trim(vim.fn.system("git rev-parse --is-inside-work-tree")) == "true"
						then
							return vim.fn.trim(vim.fn.system("basename `git rev-parse --show-toplevel`"))
						end
					end,
				},
				separator = {
					function()
						return "%="
					end,
				},
				diff = {
					"diff",
					colored = false,
				},
				diagnostics = {
					"diagnostics",
					sources = { "nvim_diagnostic" },
					diagnostics_color = {
						error = "DiagnosticError",
						warn = "DiagnosticWarn",
						info = "DiagnosticInfo",
						hint = "DiagnosticHint",
					},
					colored = true,
				},
				lsp_client = {
					function(msg)
						msg = msg or ""
						local buf_clients = vim.lsp.get_active_clients({ bufnr = 0 })

						if next(buf_clients) == nil then
							if type(msg) == "boolean" or #msg == 0 then
								return ""
							end
							return msg
						end

						local buf_ft = vim.bo.filetype
						local buf_client_names = {}

						for _, client in pairs(buf_clients) do
							if client.name ~= "null-ls" then
								table.insert(buf_client_names, client.name)
							end
						end

						vim.list_extend(buf_client_names, lsp_utils.list_formatters(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils.list_linters(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils.list_hovers(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils.list_code_actions(buf_ft))

						local hash = {}
						local client_names = {}
						for _, v in ipairs(buf_client_names) do
							if not hash[v] then
								client_names[#client_names + 1] = v
								hash[v] = true
							end
						end
						table.sort(client_names)
						return icons.ui.Code .. " " .. table.concat(client_names, ", ") .. " " .. icons.ui.Code
					end,
					colored = true,
					on_click = function()
						vim.cmd([[LspInfo]])
					end,
				},
				codeium = {
					function()
						return vim.fn["codeium#GetStatusString"]()
					end,
				},
			} -- }}}
			require("lualine").setup({
				options = {
					icons_enabled = true,
					theme = "auto",
					component_separators = { left = "", right = "" },
					section_separators = { left = "", right = "" },
					disabled_filetypes = {
						statusline = { "lazy" },
						winbar = { "help", "lazy" },
					},
					always_divide_middle = true,
					globalstatus = true,
				},
				sections = {
					lualine_a = { "mode" },
					lualine_b = { components.git_repo, "branch", components.diff, "diagnostics" },
					lualine_c = { "filename" },
					lualine_x = { components.codeium, "encoding", "fileformat", "filetype", "progress" },
					lualine_y = {},
					lualine_z = { "location" },
				},
				inactive_sections = {
					lualine_a = {},
					lualine_b = {},
					lualine_c = { "filename" },
					lualine_x = { "location" },
					lualine_y = {},
					lualine_z = {},
				},
				extensions = {},
			})
		end,
		require = { "nvim-web-devicons" },
	},
	-- }}}

	-- highlighting {{{
	{
		"andymass/vim-matchup",
		lazy = false,
		enabled = true,
		init = function()
			vim.g.matchup_matchparen_offscreen = { method = "popup" }
		end,
	},
	-- }}}

	-- pairs {{{
	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		config = function()
			local npairs = require("nvim-autopairs")
			npairs.setup({
				check_ts = true,
			})
		end,
	},
	-- }}}

	-- text objects {{{
	{
		"echasnovski/mini.ai",
		keys = {
			{ "a", mode = { "x", "o" } },
			{ "i", mode = { "x", "o" } },
		},
		dependencies = {
			{
				"nvim-treesitter/nvim-treesitter-textobjects",
				init = function()
					require("lazy.core.loader").disable_rtp_plugin("nvim-treesitter-textobjects")
				end,
			},
		},
		opts = function()
			local ai = require("mini.ai")
			return {
				n_lines = 500,
				custom_textobjects = {
					o = ai.gen_spec.treesitter({
						a = { "@block.outer", "@conditional.outer", "@loop.outer" },
						i = { "@block.inner", "@conditional.inner", "@loop.inner" },
					}, {}),
					f = ai.gen_spec.treesitter({ a = "@function.outer", i = "@function.inner" }, {}),
					c = ai.gen_spec.treesitter({ a = "@class.outer", i = "@class.inner" }, {}),
				},
			}
		end,
		config = function(_, opts_)
			local ai = require("mini.ai")
			ai.setup(opts_)
		end,
	},
	{
		"chrisgrieser/nvim-various-textobjs",
		lazy = false,
		config = function()
			require("various-textobjs").setup({ useDefaultKeymaps = true })
		end,
	},
	-- }}}

	-- tests {{{
	{
		"nvim-neotest/neotest",
		keys = {
			{
				"<leader>tn",
				"<cmd>lua require('neotest').run.run()<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>tf",
				"<cmd>lua require('neotest').run.run(vim.fn.expand('%'))<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>to",
				"<cmd>lua require('neotest').output_panel.toggle()<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>ts",
				"<cmd>lua require('neotest').summary.toggle()<cr>",
				mode = "n",
				silent = true,
			},
		},
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			"antoinemadec/FixCursorHold.nvim",
			"nvim-neotest/neotest-python",
		},
		config = function()
			require("neotest").setup({
				adapters = {
					require("neotest-python"),
				},
				output = {
					enabled = false,
					open_on_run = false,
				},
				quickfix = {
					open = false,
				},
			})
			local group = vim.api.nvim_create_augroup("NeotestConfig", {})
			vim.api.nvim_create_autocmd("FileType", {
				pattern = "neotest-output",
				group = group,
				callback = function(opts_)
					vim.keymap.set("n", "q", function()
						pcall(vim.api.nvim_win_close, 0, true)
					end, {
						buffer = opts_.buf,
					})
				end,
			})
		end,
	},
	-- }}}

	-- code annotation {{{
	{
		"danymat/neogen",
		opts = {
			snippet_engine = "luasnip",
			enabled = true,
			languages = {
				lua = {
					template = {
						annotation_convention = "ldoc",
					},
				},
				python = {
					template = {
						annotation_convention = "numpydoc",
					},
				},
			},
		},
		keys = {
			{
				"<leader>cgd",
				function()
					require("neogen").generate()
				end,
			},
			{
				"<leader>cgc",
				function()
					require("neogen").generate({ type("class") })
				end,
			},
			{
				"<leader>cgf",
				function()
					require("neogen").generate({ type("func") })
				end,
			},
		},
	},
	-- }}}

	-- vimtex {{{
	{
		"lervag/vimtex",
		ft = "tex",
		config = function()
			vim.g.tex_flavor = "latex"
			vim.g.vimtex_compiler_method = "latexmk"
			vim.g.vimtex_fold_enabled = true
			vim.g.vimtex_format_enabled = true
			vim.g.vimtex_view_enabled = true
			vim.g.vimtex_view_automatic = true
			vim.g.vimtex_view_method = "zathura"
			vim.g.vimtex_view_forward_search_on_start = false
			vim.g.vimtex_compiler_latexmk = {
				build_dir = "",
				callback = true,
				continuous = true,
				executable = "latexmk",
				hooks = {},
				options = {
					"-verbose",
					"-file-line-error",
					"-synctex=1",
					"-interaction=nonstopmode",
					"-shell-escape",
				},
			}
			vim.g.vimtex_quickfix_ignore_filters = {
				"Underfull",
				"Overfull",
				"Package balance",
				"Unused global",
				"Package fancyhdr Warning",
				"There's no line here to end",
				"No hyphenation patterns",
			}
		end,
	},
	-- }}}

	-- gitsigns {{{
	{
		"lewis6991/gitsigns.nvim",
		lazy = false,
		config = function()
			require("gitsigns").setup()
		end,
	},
	-- }}}

	-- tmux {{{
	{
		"numToStr/Navigator.nvim",
		lazy = false,
		config = function()
			require("Navigator").setup()
			vim.keymap.set("n", "<c-a>h", "<cmd>NavigatorLeft<cr>", opts)
			vim.keymap.set("n", "<c-a>j", "<cmd>NavigatorDown<cr>", opts)
			vim.keymap.set("n", "<c-a>k", "<cmd>NavigatorUp<cr>", opts)
			vim.keymap.set("n", "<c-a>l", "<cmd>NavigatorRight<cr>", opts)
			vim.keymap.set("n", "<c-s>", "<c-a>", opts)
		end,
	},
	-- }}}

	-- python {{{
	{ "kalekundert/vim-coiled-snake", ft = "python" },
	--}}}

	-- csv {{{
	{
		"chrisbra/csv.vim",
		ft = "csv",
		config = function()
			vim.g.csv_default_delim = ","
		end,
	},
	--}}}

	-- web {{{
	{
		"alvan/vim-closetag",
		ft = { "html" },
	},
	{
		"mattn/emmet-vim",
		ft = { "html" },
	},
	{
		"uga-rosa/ccc.nvim",
		cmd = { "CccPick", "CccConvert", "CccHighlighterEnable", "CccHighlighterDisable", "CccHighlighterToggle" },
		keys = {
			{ "<leader>zp", "<cmd>CccPick<cr>", opts },
			{ "<leader>zc", "<cmd>CccConvert<cr>", opts },
			{ "<leader>zh", "<cmd>CccHighlighterToggle<cr>", opts },
		},
	},
	--}}}

	-- align {{{
	{
		"Vonr/align.nvim",
		keys = {
			{ "aa", ":lua require('align').align_to_char(1, true)<cr>", mode = "x", opts },
			{ "as", ":lua require('align').align_to_char(2, true, true)<cr>", mode = "x", opts },
			{ "aw", ":lua require('align').align_to_string(false, true, true)<cr>", mode = "x", opts },
			{ "ar", ":lua require('align').align_to_string(true, true, true)<cr>", mode = "x", opts },
			{
				"gaw",
				":lua require('align').operator(require('align').align_to_string, { is_pattern = false, reverse = true, preview = true })<cr>",
				mode = "n",
				opts,
			},
			{
				"gaa",
				":lua require('align').operator(require('align').align_to_char, { length = 1, reverse = true })<cr>",
				mode = "n",
				opts,
			},
		},
	},
	-- }}}

	-- dev-container {{{
	{
		"https://codeberg.org/esensar/nvim-dev-container",
		requires = { "nvim-treesitter/nvim-treesitter" },
		config = function()
			require("devcontainer").setup({})
		end,
	},
	-- }}}
}

-- }}}

-- install and configure lazy {{{
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup(plugins, {
	defaults = { lazy = true, version = nil },
	install = { missing = true, colorscheme = { "nord" } },
	checker = { enabled = false },
	performance = {
		rtp = {
			disabled_plugins = {
				"gzip",
				"matchit",
				"matchparen",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
			paths = { vim.fn.stdpath("data") .. "/site" },
		},
	},
})
-- }}}

vim.keymap.set("n", "<leader>z", "<cmd>:Lazy<cr>", { desc = "Plugin Manager" })

-- }}}

-- keymaps {{{

-- scrolling {{{
vim.keymap.set("n", "j", "gjzz", opts)
vim.keymap.set("n", "k", "gkzz", opts)
-- }}}

-- canadian multilingual keyboard {{{
vim.keymap.set("n", "é", "/", opts)
vim.keymap.set("n", "qé", "q/", opts)
-- }}}

-- formatting {{{
vim.keymap.set("n", "<leader>s", ":%!python -m json.tool --sort-keys<cr>", opts)
-- }}}

-- empty lines {{{
vim.keymap.set("n", "<a-j>", ":set paste<cr>o<esc>k :set nopaste<cr>", opts)
vim.keymap.set("n", "<a-k>", ":set paste<cr>O<esc>j :set nopaste<cr>", opts)
-- }}}

-- comment boxes {{{
vim.keymap.set("v", "<leader>b", "!boxes<cr>", opts)
vim.cmd([[
  autocmd filetype python vnoremap <localleader>b :!boxes -d shell<cr>
]])
-- }}}

-- folding {{{
vim.keymap.set("n", "<space>", "@=(foldlevel('.') ? 'za' : '<space>')<cr>", opts)
-- }}}

-- }}}

-- languages {{{
-- python {{{
vim.cmd([[
  au BufWinEnter,BufNewFile,BufRead *.py:
      \ set softtabstop=4 |
      \ set shiftwidth=4 |
      \ set expandtab |
      \ set smarttab |
      \ set autoindent |
      \ set fileformat=unix |
      \ set foldlevel=0
]])
-- }}}

-- latex {{{
vim.cmd([[
  au BufNewFile,BufRead *.tex:
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set smarttab |
      \ set autoindent |
      \ set fileformat=unix |
      \ set textwidth=0 |
      \ set norelativenumber |
      \ set conceallevel=0 |
      \ set iskeyword-={}

  au BufNewFile,BufRead *.bib:
      \ set tabstop=2 |
      \ set softtabstop=2 |
      \ set shiftwidth=2 |
      \ set smarttab |
      \ set autoindent |
      \ set fileformat=unix |
      \ set textwidth=0 |
      \ set conceallevel=0 |
      \ set foldmethod=syntax
]])

vim.keymap.set("n", "<leader>tex", ":-1read $HOME/.config/nvim/templates/skeleton.tex<cr>", opts)

-- }}}
-- }}}
