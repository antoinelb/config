vim.keymap.set(
  "n",
  "<leader>s",
  ":setlocal spell spelllang=fr<cr>",
  { noremap = true, silent = true }
)
vim.keymap.set(
  "n",
  "<leader>tex",
  ":-1read $HOME/.config/nvim/templates/skeleton.tex<cr>",
  { noremap = true, silent = true }
)
vim.keymap.set(
  "n",
  "<leader>btex",
  ":-1read $HOME/.config/nvim/templates/beamer_skeleton.tex<cr>",
  { noremap = true, silent = true }
)
