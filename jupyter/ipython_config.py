c.TerminalInteractiveShell.confirm_exit = False

c.TerminalInteractiveShell.editing_mode = "vi"

c.TerminalInteractiveShell.editor = "nvim"

c.InteractiveShellApp.exec_lines = [r"%load_ext autoreload", "%autoreload 2"]
