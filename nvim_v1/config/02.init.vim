let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" general
filetype plugin indent on
set autochdir
set autoindent
set autoread
set clipboard=unnamedplus
set colorcolumn=80
set conceallevel=1
set cursorline
set encoding=utf-8
set expandtab
set expandtab
set gdefault
set hidden
set laststatus=0
set lazyredraw
set nobackup
set nocompatible
set nohlsearch
set noshowcmd
set noshowmode
set nrformats=
set nu
set relativenumber
set scrolloff=10
set shiftwidth=2
set showmatch
set sidescroll=10
set sidescrolloff=5
set signcolumn=yes
set softtabstop=2
set splitbelow
set tabstop=2
set textwidth=0
set undofile
syntax on

" ui
set background=dark
if (has("termguicolors"))
    set termguicolors
endif
syntax enable
syntax on
let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1
colorscheme OceanicNext
hi Normal guibg=NONE ctermbg=NONE
hi EndOfBuffer guibg=NONE ctermbg=NONE
hi Signcolumn guibg=NONE ctermbg=NONE
hi LineNr guibg=NONE ctermbg=NONE
hi SignatureMarkText guibg=NONE ctermbg=NONE
hi Error ctermfg=133 guibg=NONE ctermbg=NONE
hi AleErrorSign ctermfg=1 guibg=NONE ctermbg=NONE
hi AleWarningSign ctermfg=178 guibg=NONE ctermbg=NONE
hi Comment gui=italic cterm=italic
hi String gui=italic cterm=italic
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" definitions
set iskeyword-=-,:,#
set complete+=kspell
set listchars=tab:>-,eol:$,space:·

" change autocomplete enter behavior
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")

" commenting
let g:NERDSpaceDelims=1
let g:NERDCompactSexyComn=1
let g:NERDCommentEmptyLines=1
let g:NERDTrimTrailingWhitespace=1

" folding
set foldmethod=indent
let g:fastfold_savehook = 1

" ripgrep
set grepprg=rg\ --vimgrep

" git
let g:gitgutter_max_signs = 100

" tags
let g:easytags_suppress_ctags_warning = 1
nnoremap <c-n> <c-]>
nnoremap <c-w>n <c-w>]
let g:vim_tags_auto_generate = 0

" defx
autocmd FileType defx call s:defx_my_settings()
	function! s:defx_my_settings() abort
	  " Define mappings
	  nnoremap <silent><buffer><expr> <CR> defx#do_action('open')
	  nnoremap <silent><buffer><expr> d defx#do_action('new_directory')
	  nnoremap <silent><buffer><expr> n defx#do_action('new_file')
	  nnoremap <silent><buffer><expr> h defx#do_action('cd', ['..'])
	  nnoremap <silent><buffer><expr> ~ defx#do_action('cd')
	  nnoremap <silent><buffer><expr> s defx#do_action('toggle_select') . 'j'
	  nnoremap <silent><buffer><expr> j line('.') == line('$') ? 'gg' : 'j'
	  nnoremap <silent><buffer><expr> k line('.') == 1 ? 'G' : 'k'
endfunction

" testing
let g:test#strategy = "neovim"
let g:test#preserve_screen = 1
let g:test#python#pytest#options = {
      \ "last": "-vv"
\}

" rainbox parentheses
let g:rainbow#max_level = 16
let g:rainbow#pairs = [["(", ")"], ["[", "]"]]
augroup rainbow_lisp
  autocmd!
  autocmd! FileType python,javascript RainbowParentheses
augroup end

" syntax
autocmd BufRead,BufNewFile haproxy* set ft=haproxy
