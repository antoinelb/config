" let g:ale_emit_conflict_warnings = 0
if &compatible
 set nocompatible
endif
set runtimepath+=~/.config/nvim/plugins/repos/github.com/Shougo/dein.vim

if dein#load_state("~/.config/nvim/plugins")
  call dein#begin("~/.config/nvim/plugins")

  call dein#add("~/.config/nvim/plugins")

  " General
  call dein#add("airblade/vim-gitgutter") " Git integration
  call dein#add("christoomey/vim-tmux-navigator") " Combine vim and tmux navigation
  call dein#add("christoomey/vim-sort-motion") " Easy sorting
  call dein#add("janko-m/vim-test")  " run tests from vim
  call dein#add("jiangmiao/auto-pairs")  " completes pairs
  call dein#add("jpalardy/vim-slime") " send snippets
  call dein#add("junegunn/rainbow_parentheses.vim") " match parentheses
  call dein#add("junegunn/vim-easy-align")  " alignement
  call dein#add("Konfekt/FastFold") " faster folds
  call dein#add("kshenoy/vim-signature") " show marks in gutter
  call dein#add("mhartington/oceanic-next") " color theme
  call dein#add("sbdchd/neoformat") " formatter
  call dein#add("scrooloose/nerdcommenter") " automatic commenting
  call dein#add("Shougo/context_filetype.vim")  " find file contexts in other files
  call dein#add("Shougo/denite.nvim")  " fuzzy file searching
  call dein#add("Shougo/deoplete.nvim", {
        \ "do": ":UpdateRemotePlugins"
        \ }) " autocompletion
  call dein#add("Shougo/echodoc.vim") " function def in status bar
  call dein#add("Shougo/neco-vim") " autocomplete deoplete
  call dein#add("Shougo/neosnippet.vim")  " snippets
  call dein#add("Shougo/neosnippet-snippets")  " snippets
  call dein#add("tmhedberg/matchit")  " extra % matching
  call dein#add("tpope/vim-fugitive") " git integration
  call dein#add("tpope/vim-surround") " modify surrounding chars
  call dein#add("vim-airline/vim-airline") " airline
  call dein#add("vim-airline/vim-airline-themes") " themes for airline
  call dein#add("vim-scripts/camelcasemotion")  " move to underscores or camelcase
  call dein#add("w0rp/ale") " linting
  call dein#add("Yggdroot/indentline") " show indentation
  " Syntax
  call dein#add("infobip/haproxy.vim") 
  call dein#add("vim-scripts/nginx.vim") " Nginx highlighting
  call dein#add("cespare/vim-toml") " TOML syntax
  " Python
  call dein#add("davidhalter/jedi")  " jedi for vim
  call dein#add("deoplete-plugins/deoplete-jedi")  " autocompletion for python
  call dein#add("fs111/pydoc.vim") " pydoc
  call dein#add("tmhedberg/SimpylFold") " code folding
  call dein#add("vim-scripts/indentpython.vim") " indentation for python
  " Latex
  call dein#add("lervag/vimtex") " latex mode
  " Web
  call dein#add("carlitux/deoplete-ternjs")  " autocomplete for js
  call dein#add("elzr/vim-json") " Better JSON syntax
  call dein#add("mattn/emmet-vim")  " Emmet
  call dein#add("mxw/vim-jsx") " JSX highlighting 
  call dein#add("othree/html5.vim") " HTML5 highlighting
  call dein#add("othree/xml.vim") " XML and HTML editing
  call dein#add("pangloss/vim-javascript") " Javascript syntax and indentation
  call dein#add("vim-scripts/svg.vim") " SVG syntax
  " SQL
  call dein#add("vim-scripts/SQLUtilities") " SQL functions
  " R
  call dein#add("jalvesaq/Nvim-R")  " R mode
  " CSV
  call dein#add("chrisbra/csv.vim")  " CSV mode
  " Julia
  call dein#add("JuliaEditorSupport/julia-vim")  " Julia mode
  " Markdown
  call dein#add("plasticboy/vim-markdown")  " Markdown mode
  
  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable

if dein#check_install()
  call dein#install()
endif
