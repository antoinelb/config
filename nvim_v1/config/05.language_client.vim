let g:LanguageClient_serverCommands = {}
" let g:LanguageClient_serverCommands = {
      " \ "python": ["/usr/bin/pyls"],
      " \ "julia": ["julia", "--startup-file=no", "--history-file=no", "-e", "
      " \   using LanguageServer;
      " \   using Pkg;
      " \   import StaticLint;
      " \   import SymbolServer;
      " \   env_path = dirname(Pkg.Types.Context().env.project_file);
      " \   debug = false;
      " \
      " \   server = LanguageServer.LanguageServerInstance(stdin, stdout, debug, env_path, '', Dict());
      " \   server.runlinter = true;
      " \   run(server);
      " \ "]
      " \ }

let g:LanguageClient_autoStart = 1
let g:LanguageClient_rootMarkers = {}

set completefunc=LanguageClient#complete

let g:LanguageClient_loadSettings = 1
