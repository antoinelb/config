let g:ale_completion_enabled = 0
let g:ale_linters_explicit = 1
let g:ale_sign_column_alway = 1
let g:airline#extensions#ale#enabled = 1
" let g:ale_linters = {
      " \ 'python': ["flake8", 'mypy', 'pylint'],
      " \ 'js': ['eslint'],
      " \ "css": ["stylelint"],
      " \ "scss": ["stylelint"]
      " \ }
let g:ale_linters = {
      \ 'python': ["flake8", 'pylint'],
      \ 'js': ['eslint'],
      \ "css": ["stylelint"],
      \ "scss": ["stylelint"]
      \ }
let g:ale_linter_aliases = {'js': 'css'}
let g:ale_echo_msg_format = '[%linter%] %code: %%s'
let g:ale_python_pylint_executable = "python (which pylint)"
let g:ale_python_pylint_options = "--load-plugins pylint_django"
