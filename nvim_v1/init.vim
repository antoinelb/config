" vim: set foldmethod=marker foldlevel=0 nomodeline:
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" =============================================================================
" Plugins {{{
" =============================================================================

if &compatible
 set nocompatible
endif
set runtimepath+=~/.config/nvim/plugins/repos/github.com/Shougo/dein.vim

call plug#begin("~/.config/nvim/plugins")

" general {{{

" git integration
Plug 'airblade/vim-gitgutter'

" search unicode characters
Plug 'chrisbra/unicode.vim'

" easy sorting
Plug 'christoomey/vim-sort-motion'

" combine vim and tmux navigation
Plug 'christoomey/vim-tmux-navigator'

" theme 
Plug 'arcticicestudio/nord-vim'

" move using keys
Plug 'easymotion/vim-easymotion'

" completes pairs
Plug 'jiangmiao/auto-pairs'

" asynchronous fuzzy finder
Plug 'junegunn/fzf', { 'build': './install', 'merged': 0 }
Plug 'junegunn/fzf.vim'

" faster folds
Plug 'Konfekt/FastFold'

" show marks in gutter
Plug 'kshenoy/vim-signature'

" align on characters
Plug 'junegunn/vim-easy-align'

" autocompletion
Plug 'neoclide/coc.nvim', { 'branch': 'release' }

" easy commenting
Plug 'numToStr/Comment.nvim'

" find file contexts in other files
Plug 'Shougo/context_filetype.vim'

" snippets
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'

" support . for some plugins
Plug 'tpope/vim-repeat'

" git integration
Plug 'tpope/vim-fugitive'

" modify surrounding chars
Plug 'tpope/vim-surround'

" airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" movement keys work with snake and camel case
Plug 'vim-scripts/camelcasemotion'

" run tests from vim
Plug 'vim-test/vim-test'

" show indentation
Plug 'Yggdroot/indentline'

" find where maps are defined
Plug 'vim-scripts/listmaps.vim'

" }}}

" syntax {{{

let g:polyglot_disabled = ['autoindent', 'latex']
Plug 'sheerun/vim-polyglot'

" }}}

" python {{{

" code folding
Plug 'kalekundert/vim-coiled-snake'
" Plug 'tmhedberg/SimpylFold'

" }}}

" latex {{{

Plug 'lervag/vimtex'

" }}}

" web {{{

" auto close tags
Plug 'alvan/vim-closetag'

" emmet
Plug 'mattn/emmet-vim'

" }}}

" sql {{{

" sql functions
Plug 'vim-scripts/SQLUtilities'

" }}}

" r {{{

" r mode
Plug 'jalvesaq/Nvim-R'

" }}}

" csv {{{

" csv mode
Plug 'chrisbra/csv.vim'

" }}}

" markdown {{{

" markdown mode
Plug 'plasticboy/vim-markdown'

" }}}

" writing {{{

" french grammar
Plug 'vim-scripts/LanguageTool'

" note taking wiki
Plug 'vimwiki/vimwiki', {'branch': 'dev'}
" }}}

" julia {{{

" julia syntax
" Plug 'JuliaEditorSupport/julia-vim'

" }}}

call plug#end()

" }}}
" =============================================================================
" Plugin settings {{{
" =============================================================================

" fzf {{{

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

let g:fzf_tags_command = 'ctags -R --kinds-JavaScript=-mpcgGS'

" }}}

" vimwiki {{{

let g:vimwiki_list = [
      \{
      \ "path": "~/documents/notes/", 
      \ "syntax": "markdown",
      \ "ext": ".md"
      \},
      \{
      \ "path": "~/documents/research/papers", 
      \ "syntax": "markdown",
      \ "ext": ".md"
      \}
      \]
let g:vimwiki_global_ext = 0
let g:vimwiki_filetypes = ['markdown']
let g:vimwiki_folding = 'custom'
au filetype vimwiki set filetype=vimwiki.pandoc

let g:vimwiki_key_mappings =
\ {
\   'all_maps': 1,
\   'global': 1,
\   'headers': 1,
\   'text_objs': 1,
\   'table_format': 1,
\   'table_mappings': 0,
\   'lists': 0,
\   'links': 1,
\   'html': 1,
\   'mouse': 0,
\ }

" }}}

" context_filetype {{{
if !exists('g:context_filetype#same_filetypes')
  let g:context_filetype#filetypes = {}
endif
let g:context_filetype#filetypes.svelte =
      \ [
      \   {'filetype': 'javascript', 'start': '<script>', 'end': '</script>'},
      \   {'filetype': 'css', 'start': '<style>', 'end': '</style>'},
      \ ]

" }}}

" slime {{{

let g:slime_target = "tmux"
let g:slime_paste_file = "/tmp/slime_paste"
let g:slime_default_config = {
      \"socket_name": get(split($TMUX, ","), 0),
      \"target_pane": ":.2"
      \}
let g:slime_python_ipython = 1

" }}}

" ale {{{

let g:ale_linters_explicit = 1
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = "normal"
let g:ale_sign_column_always = 1
let g:airline#extensions#ale#enabled = 1
let g:ale_linters = {
      \ 'python': ['bandit', 'flake8', 'mypy', 'pylint'],
      \ 'javascript': ['eslint'],
      \ 'javascriptreact': ['eslint'],
      \ 'css': ['stylelint'],
      \ 'scss': ['stylelint'],
      \ 'html': ['prettier'],
      \ 'elm': ['elm_ls']
      \ }
" let g:ale_python_pylint_options = "--load-plugins pylint_django"
let g:ale_echo_msg_format = '[%linter%] %code: %%s'

let g:ale_python_bandit_options = "-s B101"
let g:ale_python_black_options = "-l 79"

let g:ale_fix_on_save = 1
let g:ale_fixers = {
      \ 'javascript': ['prettier'],
      \ 'javascriptreact': ['prettier'],
      \ 'python': ['isort', 'black'],
      \ 'css': ['prettier', 'stylelint'],
      \ 'scss': ['prettier', 'stylelint'],
      \ 'json': ['prettier'],
      \ 'html': ['prettier'],
      \ 'markdown': ['prettier'],
      \ 'elm': ['elm-format'],
      \ 'rescript': [
      \   {buffers -> {
      \     'command': 'bsc -color never -format %t'
      \   }}
      \ ]
\}


" }}}

" jupytext {{{

let g:jupytext_fmt = 'py:light'

" }}}

" indentline {{{

let g:indentLine_conceallevel = &conceallevel

" }}}

" fugitive {{{

nnoremap <leader>ga :Gw<cr>
nnoremap <leader>gc :Git commit<cr>

" }}}

" neosnippets {{{

imap <c-k> <plug>(neosnippet_expand_or_jump)
smap <c-k> <plug>(neosnippet_expand_or_jump)
xmap <c-k> <plug>(neosnippet_expand_target)

smap <expr><tab> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

" }}}

" commenting {{{

lua require('Comment').setup()

" }}}

" }}}
" =============================================================================
" General settings {{{
" =============================================================================

set autochdir             " change dir to current file dir
set backupcopy=yes        " disables safe write to enable hot module replacement
set encoding=utf-8        " displayed encoding
set fileencoding=utf-8    " written encoding
filetype plugin indent on " activate detection of filetypes
syntax on                 " syntax highlight
set autoindent            " new line indent is same as previous
set autoread              " automatically read file that has been changed outside vim
set clipboard=unnamedplus " have clipboard be system clipboard
set colorcolumn=80        " put colored column
set cursorline            " find current line quickly
set expandtab             " change tab to spaces
set gdefault              " makes all substitute be global
set hidden                " allow buffer switching with unsave files
set laststatus=1          " adds status if at least 2 files
set lazyredraw            " redraw only when necessary
set nohlsearch            " disable highlight search
set noshowcmd             " don't show last command as last line of screen
set noshowmode            " don't show the mode as last line
set nrformats=alpha       " characters are considered for incrementing
set number                " show line numbers
set previewheight=5       " Height of the preview window
set relativenumber        " line number is relative to cursor
set scrolloff=10          " minimum number of lines to keep below and above cursor
set shiftwidth=2          " tab width
set showmatch             " show matching bracket
set signcolumn=yes        " if sign column should always be present
set softtabstop=2         " tab width
set splitbelow            " vertical split is always below
set undofile              " save undos once file is closed
set undolevels=1000       " number of undos to save
set undoreload=10000      " number of lines to save for undo
set updatetime=300        " update time delay
set tabstop=2             " tab width
set textwidth=0           " disable limit of chars to paste
set wrapmargin=0          " prevent line breaks

" definitions
set iskeyword-=-,:,#
set listchars=tab:>-,eol:$,space:·

set wildignore+=*.pyc,*/__pycache__/*
set wildignore+=*.swp,~*
set wildignore+=*.zip,*.tar
set wildignore+=*.bs.js

set dictionary+=/usr/share/dict/words
set dictionary+=/usr/share/dict/french

let mapleader = ','

" }}}
" =============================================================================
" Specific settings {{{
" =============================================================================

" autocomplete {{{

inoremap <expr> <cr> (pumvisible() ? "\<c-y>\<cr>" : "\<cr>")

" tab to switch options
inoremap <silent><expr> <tab>
      \ pumvisible() ? "\<c-n>" :
      \ CheckBackspace() ? "\<tab>" :
      \ coc#refresh()
inoremap <silent><expr><s-tab> pumvisible() ? "\<c-p>" : "\<c-h>"
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1] =~# '\s'
endfunction

" trigger completion
inoremap <silent><expr> <c-space> coc#refresh()

" enter to select
inoremap <silent><expr><cr> pumvisible() ? "\<c-y>\<cr>" : "\<cr>"

" error navigation
nmap <silent> <C-h> <plug>(coc-diagnostic-prev)
nmap <silent> <C-l> <plug>(coc-diagnostic-next)

" definition and usage navigation
nmap <silent> gd <plug>(coc-definition)
nmap <silent> gr <plug>(coc-references)

" show documentation
nnoremap <silent> K :call <sid>show_documentation()<cr>
function! s:show_documentation()
  if (index(['vim', 'help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . ' ' . expand('<cword')
  endif
endfunction

" rename symbol
nmap <leader>rn <plug>(coc-rename)

" options
set completeopt-=preview
set completeopt+=noinsert
set completeopt+=noselect
set completeopt+=menuone
set shortmess+=c

" format
nnoremap <silent> <leader>= :call CocAction('format')<cr>

" scroll preview window
nnoremap <silent><nowait><expr> <c-y> coc#float#has_scroll() ? coc#float#scroll(1): "\<c-y>"
nnoremap <silent><nowait><expr> <c-e> coc#float#has_scroll() ? coc#float#scroll(0): "\<c-e>"

" }}}


" folding {{{

let g:fastfold_savehook = 1

" }}}

" fuzzy finder {{{

set grepprg=rg\ --vimgrep

" }}}

" git {{{

let g:gitgutter_max_signs = 100

" }}}

" neoterm {{{

let g:neoterm_default_mod = ":rightbelow"
let g:neoterm_autoscroll = 1

" }}}

" testing {{{

let g:test#strategy = "neovim"
let g:test#preserve_screen = 1
let g:test#python#pytest#options = {
      \ "last": "-vv"
\}

" }}}

" tmux {{{

autocmd BufReadPost,FileReadPost,BufNewFile,BufEnter * call system("tmux rename-window 'nvim|" . expand("%:t") . "'")
autocmd VimLeave * call system("tmux rename-window 'zsh'")

" }}}

" language server {{{

set hidden

let g:LanguageClient_serverCommands = {
      \ 'reason': ['/usr/bin/reason-language-server'],
      \ }
let g:LanguageClient_hoverPreview = 'Never'
let g:LanguageClient_useVirtualText = 'No'
let g:LanguageClient_diagnosticsEnable = 0

" }}}

" netrw {{{

let g:netre_liststyle = 3

" }}}

" fzf {{{

nnoremap <c-f> :Files<cr>
nnoremap <c-g> :GFiles<cr>
nnoremap <c-t> :BTags<cr>
nnoremap <c-p> :Rg<cr>
nnoremap <c-b> :Buffers<cr>

" }}}

" coc {{{

nnoremap <leader>f <cmd>CocCommand explorer<cr>

" }}}

" language {{{

autocmd FileType tex,markdown nnoremap <LocalLeader>s :setlocal spell spelllang=fr<cr>

" }}}

" }}}
" =============================================================================
" Language settings {{{
" =============================================================================

" python {{{
let g:python3_host_prog='/usr/bin/python3'
let g:python2_host_prog='/usr/bin/python2'
let g:python_host_prog='/usr/bin/python'

au BufNewFile,BufRead *.py:
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set expandtab |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set foldlevel=1 |
    \ set textwidth=0 |
    \ set foldmethod=indent

let python_highlight_all=1

augroup PythonCustomization
  " highlight python self, when followed by a comma, a period or a parenth
   :autocmd FileType python syn match pythonStatement "\(\W\|^\)\@<=self\([\.,)]\)\@="
augroup END

autocmd filetype python vnoremap <localleader>b :!boxes -d shell<cr>

au FileType python let b:coc_root_patterns = ['.venv', 'pyproject.toml']

" sort imports
autocmd bufwritepre *.py silent! :call CocAction('runCommand', 'python.sortImports')
" autocmd bufwritepre *.py :! isort %

" django
autocmd filetype html nnoremap <silent> <localleader>dj :set ft=htmldjango<cr>


" }}}

" latex {{{

let maplocalleader=","
let g:tex_flavor='latex'
let g:vimtex_compiler_method='latexmk'
let g:vimtex_fold_enabled = 1
let g:vimtex_format_enabled = 1
let g:vimtex_view_enabled = 1
let g:vimtex_view_automatic = 1
let g:vimtex_view_method = "zathura"
let g:vimtex_view_forward_search_on_start = 0
let g:vimtex_compiler_latexmk = {
    \ 'build_dir' : '',
    \ 'callback' : 1,
    \ 'continuous' : 1,
    \ 'executable' : 'latexmk',
    \ 'hooks' : [],
    \ 'options' : [
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \   '-shell-escape'
    \ ],
    \}
let g:vimtex_quickfix_ignore_filters = [
      \ 'Underfull',
      \ 'Overfull',
      \ 'Package balance'
      \]

au BufNewFile,BufRead *.tex:
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set textwidth=0 |
    \ set norelativenumber |
    \ set conceallevel=0 |
    \ set iskeyword-={}

au BufNewFile,BufRead *.bib:
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set textwidth=0 |
    \ set conceallevel=0 |
    \ set foldmethod=syntax

au FileType tex let b:AutoPairs = AutoPairsDefine({'$' : '$'})

let g:tex_conceal = ""
let g:vimtex_imaps_leader = "`"

nnoremap ,tex :-1read $HOME/.config/nvim/templates/skeleton.tex<cr>
nnoremap ,btex :-1read $HOME/.config/nvim/templates/beamer_skeleton.tex<cr>

" }}}

" markdown {{{

autocmd BufNewFile,BufReadPost *.md set filetype=markdown
let g:vim_markdown_conceal = 0
let g:vim_markdown_math = 1
let g:vim_markdown_new_list_item_indent = 0

au BufNewFile,BufRead *.md,*.txt:
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set expandtab |
    \ set smarttab |
    \ set fileformat=unix |
    \ set conceallevel=0 |
    \ set textwidth=0 |
    \ set foldmethod=syntax

" }}}

" web {{{

au BufNewFile,BufRead *.js,*.html,*.css,*.json,*.jsx,*.scss:
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set conceallevel=1

au BufNewFile,BufRead *.css,*.json,*.jsx,*.scss setlocal
    \ foldmethod=marker
    \ foldmarker={,}

au BufNewFile,BufRead *.html setlocal
    \ foldmethod=indent
    " \ ft=jinja

augroup javascript_folding
  au!
  au FileType javascript setlocal foldmethod=syntax
augroup end


let g:javascript_plugin_flow = 1

" allow jsx in js files
let g:jsx_ext_required = 0

" html5 syntax options
let g:html5_event_handler_attributes_complete = 0
let g:html5_rdfa_attributes_complete = 0
let g:html5_microdata_attributes_complete = 0
let g:html5_aria_attributes_complete = 0

let g:xmledit_enable_html = 1

function HtmlAttribCallback(xml_tag)
  return 0
endfunction

let g:user_emmet_install_global = 0
autocmd FileType js,html,css,htmldjango,scss,reason,javascriptreact EmmetInstall
let g:user_emmet_settings = {
\}
let g:user_emmet_settings = {
      \ "html": {
        \ "block_all_childless": 1
      \},
      \ 'typescript': {
      \   'extends': 'jsx',
        \ "block_all_childless": 1
      \},
      \ 'reason': {
        \ 'extends': 'jsx',
        \ "block_all_childless": 1
      \}
      \}

let g:closetag_filenames = '*.html,*.tsx,*.re,*.jsx,*.js'
let g:closetag_close_shortcut = '<leader>>'

let g:user_emmet_leader_key="<c-e>"
" let g:user_emmet_expandabbr_key="<c-e>"

" }}}

" sql {{{

let g:sqlutil_keyword_case = '\U'
let g:sqlutil_align_comma = 1
let g:sqlutil_align_first_word = 1
let g:sqlutil_align_keyword_right = 1
let g:sqlutil_align_where = 1
autocmd bufwritepre,filewritepre *.sql silent execute ":%SQLUFormatter<cr>"

" }}}

" r {{{

au BufNewFile,BufRead *.R,*.r:
    \ set foldmethod=syntax |
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set conceallevel=1

let R_assign = 0
let R_min_editor_width = 80
let R_rconsole_width = 1000
let R_show_args = 0

" }}}

" bash {{{

au FileType sh:
      \ set foldmethod=syntax |
      \ let g:is_bash=1 |
      \ let g:sh_fold_enabled=5

" }}}

" yaml {{{

au BufNewFile,BufRead *.yml,*.yaml:
      \ set foldmethod=indent |
      \ set foldlevel=0

" }}}

" elm {{{

autocmd filetype elm setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=indent

let g:ale_elm_ls_use_global = 1
let g:ale_elm_ls_elm_analyse_trigger = 'change'

" }}}

" }}}
" =============================================================================
" Ui {{{
" =============================================================================

if (has("termguicolors"))
    set termguicolors
endif
let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1

set background=dark
colorscheme nord

hi LineNr guibg=NONE ctermbg=NONE
hi Normal guibg=NONE ctermbg=NONE
hi EndOfBuffer guibg=NONE ctermbg=NONE
hi Signcolumn guibg=NONE ctermbg=NONE
hi SignatureMarkText guibg=NONE ctermbg=NONE
hi Error ctermfg=133 guibg=NONE ctermbg=NONE
hi AleErrorSign ctermfg=1 guibg=NONE ctermbg=NONE
hi AleWarningSign ctermfg=178 guibg=NONE ctermbg=NONE
hi Comment gui=italic cterm=italic
hi String gui=italic cterm=italic
hi Folded guifg=darkgray ctermfg=darkgray
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" hi clear SpellBad
" hi SpellBad guibg=#641919

" status bar {{{

let g:airline_theme = 'nord'

" don't overwrite symbols if they already exist
if !exists("g:airline_symbols")
  let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" use icons
let g:airline_powerline_fonts = 1

let g:airline_section_c = '%t'


" }}}


" }}}
" =============================================================================
" Mappings {{{
" =============================================================================

" scrolling {{{

nnoremap j gjzz
nnoremap k gkzz

" }}}

" folding {{{

nnoremap zuz <plug>(FastFoldUpdate)
nnoremap <silent> <space> @=(foldlevel('.')?'za':"\<space>")<cr>

" }}}

" easy-align {{{

xmap ga <plug>(EasyAlign)
nmap ga <plug>(EasyAlign)

" }}}
 
" canadian multilingual keyboard {{{

nnoremap é /
nnoremap qé q/

" }}}

" formatting {{{

nmap <silent> <leader>s :%!python -m json.tool --sort-keys<cr>

" }}}

" empty lines {{{

nnoremap <silent><A-j> :set paste <cr>o<esc>k:set nopaste<cr>
nnoremap <silent><A-k> :set paste <cr>O<esc>j:set nopaste<cr>

" }}}

" snake and camel case {{{

map w <Plug>CamelCaseMotion_w
map b <Plug>CamelCaseMotion_b
map e <Plug>CamelCaseMotion_e
sunmap w
sunmap b
sunmap e

" }}}

" comment boxes {{{

vmap <leader>b !boxes<cr>

" }}}

" tmux {{{

let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-a>h :TmuxNavigateLeft<cr>
nnoremap <silent> <c-a>j :TmuxNavigateDown<cr>
nnoremap <silent> <c-a>k :TmuxNavigateUp<cr>
nnoremap <silent> <c-a>l :TmuxNavigateRight<cr>
nnoremap <silent><c-s> <c-a>

" }}}

" testing {{{

nmap <silent> <leader>tn :TestNearest<cr>
nmap <silent> <leader>tf :TestFile<cr>
nmap <silent> <leader>tl :TestLast<cr>
tmap <c-o> <c-\><c-n>

" }}}

" easymotion {{{

nmap <leader><leader>w <plug>(easymotion-w)

" }}}

" unicode {{{

imap <c-u> <plug>(UnicodeFuzzy)


" }}}

" }}}

