-- vim: set foldmethod=marker foldlevel=0 nomodeline:

-- {{{ options
vim.g.mapleader = ","
vim.g.maplocalleader = ","

vim.opt.autochdir = true -- change dir to current file dir
vim.opt.autoindent = true -- new line indent is same as previous
vim.opt.autoread = true -- automatically read file that has been changed outside vim
vim.opt.backupcopy = "yes" -- disables safe write to enable hot module replacement
vim.opt.breakindent = true -- line wrap keeps indentation
vim.opt.clipboard = "unnamedplus" -- have clipboard be system clipboard
vim.opt.cmdheight = 0 -- height of the command section
vim.opt.colorcolumn = "80" -- put colored column
vim.opt.completeopt = "menuone,noselect" -- options for complete menus
vim.opt.conceallevel = 0 -- amount visual should reflect what it is
vim.opt.cursorline = true -- find current line quickly
vim.opt.encoding = "utf-8" -- displayed encoding
vim.opt.expandtab = true -- change tab to spaces
vim.opt.filetype = "on" -- detect filetypes
vim.opt.foldexpr = "nvim_treesitter#foldexpr()" -- method to use for folding
vim.opt.foldlevel = 0 -- initial fold level
vim.opt.foldmethod = "expr" -- method to use for folding
vim.opt.formatoptions = "jcroqlnt" -- various options for formatting
vim.opt.gdefault = true -- makes all substitute be global
vim.opt.hidden = true -- allow buffer switching with unsave files
vim.opt.hlsearch = false -- disable highlight search
vim.opt.laststatus = 1 -- adds status if at least 2 files
vim.opt.nrformats = "alpha" -- characters are considered for incrementing
vim.opt.number = true -- show line numbers
vim.opt.previewheight = 5 -- Height of the preview window
vim.opt.relativenumber = true -- line number is relative to cursor
vim.opt.scrolloff = 10 -- minimum number of lines to keep below and above cursor
vim.opt.shiftwidth = 2 -- tab width
vim.opt.showcmd = false -- don't show last command as last line of screen
vim.opt.showmatch = true -- show matching bracket
vim.opt.showmode = false -- don't show the mode as last line
vim.opt.signcolumn = "yes" -- if sign column should always be present
vim.opt.softtabstop = 2 -- tab width
vim.opt.splitbelow = true -- vertical split is always below
vim.opt.swapfile = false -- disable swap file
vim.opt.tabstop = 2 -- tab width
vim.opt.termguicolors = true -- use terminal colors
vim.opt.textwidth = 0 -- disable limit of chars to paste
vim.opt.timeoutlen = 300 -- time to wait for mapped sequence to complete
vim.opt.undofile = true -- save undos once file is closed
vim.opt.undolevels = 1000 -- number of undos to save
vim.opt.undoreload = 10000 -- number of lines to save for undo
vim.opt.updatetime = 250 -- update time delay
vim.opt.wildmode = "longest:full,full" -- mode for finding files
vim.opt.wrapmargin = 0 -- prevent line breaks

vim.opt.iskeyword:remove("-,:,#")
vim.opt.listchars:append("space:·")
vim.opt.listchars:append("eol:↴")

vim.opt.wildignore:append("*.pyc")
vim.opt.wildignore:append("**/__pycache__/*")
vim.opt.wildignore:append("*.zip")
vim.opt.wildignore:append("*.tar")

vim.opt.dictionary:append("/usr/share/dict/words")
vim.opt.dictionary:append("/usr/share/dict/french")

-- disable other users of ,
vim.keymap.set({ "n", "v" }, ",", "<nop>", { silent = true })
-- }}}

-- {{{ autocmds

local function augroup(name)
	return vim.api.nvim_create_augroup("nde_" .. name, { clear = true })
end

-- show yanked text {{{
vim.api.nvim_create_autocmd("TextYankPost", {
	callback = function()
		vim.highlight.on_yank()
	end,
	group = augroup("highlight_yank"),
	pattern = "*",
})
-- }}}

-- go to last location when opening buffer {{{
vim.api.nvim_create_autocmd("BufReadPost", {
	group = augroup("last_loc"),
	callback = function()
		local mark = vim.api.nvim_buf_get_mark(0, '"')
		local lcount = vim.api.nvim_buf_line_count(0)
		if mark[1] > 0 and mark[1] <= lcount then
			pcall(vim.api.nvim_win_set_cursor, 0, mark)
		end
	end,
})
-- }}}

-- close windows when closing file {{{
vim.api.nvim_create_autocmd("FileType", {
	group = augroup("close_with_q"),
	pattern = {
		"OverseerForm",
		"OverseerList",
		"checkhealth",
		"floggraph",
		"fugitive",
		"git",
		"gitcommit",
		"help",
		"lspinfo",
		"man",
		"neotest-output",
		"neotest-summary",
		"qf",
		"query",
		"spectre_panel",
		"startuptime",
		"toggleterm",
		"tsplayground",
		"vim",
		"neoai-input",
		"neoai-output",
	},
	callback = function(event)
		vim.bo[event.buf].buflisted = false
		vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
	end,
})
-- }}}

-- disable comments on new lines {{{
vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
	group = augroup("auto_format_options"),
	callback = function()
		vim.cmd("set formatoptions-=cro")
	end,
})
-- }}}

-- }}}

-- {{{ keymaps

local keymap_opts = { silent = true, noremap = true }

-- auto indent {{{
vim.keymap.set("n", "i", function()
	if #vim.fn.getline(".") == 0 then
		return [["_cc]]
	else
		return "i"
	end
end, { expr = true })
-- }}}

-- scrolling {{{
vim.keymap.set("n", "j", "gjzz", keymap_opts)
vim.keymap.set("n", "k", "gkzz", keymap_opts)
-- }}}

-- canadian multilingual keyboard {{{
vim.keymap.set("n", "é", "/", keymap_opts)
vim.keymap.set("n", "qé", "q/", keymap_opts)
-- }}}

-- formatting {{{
vim.keymap.set("n", "<leader>s", ":%!python -m json.tool --sort-keys<cr>", keymap_opts)
-- }}}

-- empty lines {{{
vim.keymap.set("n", "<a-j>", ":set paste<cr>o<esc>k :set nopaste<cr>", keymap_opts)
vim.keymap.set("n", "<a-k>", ":set paste<cr>O<esc>j :set nopaste<cr>", keymap_opts)
-- }}}

-- comment boxes {{{
vim.keymap.set("v", "B", "!boxes<cr>", keymap_opts)
vim.cmd([[
  autocmd filetype python vnoremap B :!boxes -d shell<cr>
]])
-- }}}

-- folding {{{
vim.keymap.set("n", "<space>", "@=(foldlevel('.') ? 'za' : '<space>')<cr>", keymap_opts)
-- }}}

-- quickfix {{{
vim.keymap.set("n", "<leader>q", "<cmd>ccl<cr>", keymap_opts)
-- }}}

-- indenting {{{
vim.keymap.set("n", "<tab>", ">>", keymap_opts)
vim.keymap.set("n", "<s-tab>", "<<", keymap_opts)
vim.keymap.set("v", "<tab>", ">gv", keymap_opts)
vim.keymap.set("v", "<s-tab>", "<gv", keymap_opts)
-- }}}

-- }}}

-- {{{ plugins
-- lsp utils {{{

local lsp_utils = {}

function lsp_utils.capabilities()
	local capabilities = vim.lsp.protocol.make_client_capabilities()
	capabilities.textDocument.foldingRange = {
		dynamicRegistration = false,
		lineFoldingOnly = true,
	}
	return require("cmp_nvim_lsp").default_capabilities(capabilities)
end

function lsp_utils.on_attach(on_attach)
	vim.api.nvim_create_autocmd("LspAttach", {
		callback = function(args)
			local bufnr = args.buf
			local client = vim.lsp.get_client_by_id(args.data.client_id)
			on_attach(client, bufnr)
		end,
	})
end

local diagnostics_active = false

function lsp_utils.show_diagnostics()
	return diagnostics_active
end

function lsp_utils.toggle_diagnostics()
	diagnostics_active = not diagnostics_active
	if diagnostics_active then
		vim.diagnostic.show()
	else
		vim.diagnostic.hid()
	end
end

function lsp_utils.opts(name)
	local plugin_ = require("lazy.core.config").plugins[name]
	if not plugin_ then
		return {}
	end
	local Plugin = require("lazy.core.plugin")
	return Plugin.values(plugin_, "opts", false)
end

-- }}}

local plugins = {
	-- utilities {{{
	{ "tpope/vim-repeat", event = "VeryLazy" },
	{ "nvim-lua/plenary.nvim", event = "VeryLazy" },
	-- }}}

	-- git {{{
	{
		"TimUntersberger/neogit",
		cmd = "Neogit",
		keys = {
			{ "<leader>gs", "<cmd>Neogit kind=floating<cr>", desc = "Status" },
		},
		config = function()
			require("neogit").setup({
				disable_commit_confirmation = false,
				popup = {
					kind = "replace",
				},
			})
		end,
	},
	{
		"lewis6991/gitsigns.nvim",
		lazy = false,
		config = function()
			require("gitsigns").setup()
		end,
	},
	-- }}}

	-- ui {{{
	{
		"nvim-tree/nvim-web-devicons",
		config = { default = true },
	},
	{
		"shaunsingh/nord.nvim",
		lazy = false,
		config = function()
			vim.g.nord_contrast = true
			vim.g.nord_borders = true
			vim.g.nord_disable_background = true
			vim.g.nord_italic = true
			vim.g.nord_bold = true
			require("nord").set()
		end,
	},
	{
		"stevearc/dressing.nvim",
		event = "VeryLazy",
		opts = {},
	},
	{
		"rcarriga/nvim-notify",
		event = "VeryLazy",
		opts = {
			background_colour = "#000000",
		},
	},
	-- }}}

	-- editor {{{
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			{ "nvim-telescope/telescope-file-browser.nvim" },
			{ "stevearc/aerial.nvim" },
		},
		cmd = "Telescope",
		keys = {
			{ "<c-f>", "<cmd>Telescope find_files<cr>" },
			{ "<c-g>", "<cmd>Telescope git_files<cr>" },
			{ "<c-p>", "<cmd>Telescope live_grep<cr>" },
			{ "<c-t>", "<cmd>Telescope aerial<cr>" },
			{
				"<c-b>",
				"<cmd>lua require('telescope.builtin').buffers({ sort_lastused = true, ignore_current_buffer = true})<cr>",
				desc = "Buffers",
			},
			{ "<c-d>", "<cmd>Telescope file_browser grouped=true<cr>", desc = "Find files (grouped)" },
		},
		opts = {
			extensions = {
				file_browser = {
					hijack_netrw = true,
				},
			},
			defaults = {
				file_ignore_patterns = {
					"node_modules",
					"*.app",
					"*.aux",
					"*.bbl",
					"*.bcf",
					"*.blg",
					"*.dvi",
					"*.fdb_latexmk",
					"*.fls",
					"*.glo",
					"*.ist",
					"*.lof",
					"*.log",
					"*.lot",
					"*.nlo",
					"*.out",
					"*.run.xml",
					"*.sta",
					"*.synctex*",
					"*.synctex.gz",
					"*.tdo",
					"*.toc",
					"*.xcp",
				},
				mappings = {
					i = {
						["<C-j>"] = function(...)
							require("telescope.actions").move_selection_next(...)
						end,
						["<C-k>"] = function(...)
							require("telescope.actions").move_selection_previous(...)
						end,
						["<C-n>"] = function(...)
							require("telescope.actions").cycle_history_next(...)
						end,
						["<C-p>"] = function(...)
							require("telescope.actions").cycle_history_prev(...)
						end,
					},
				},
			},
		},
		config = function(_, opts)
			local telescope = require("telescope")
			telescope.setup(opts)
			telescope.load_extension("aerial")
			telescope.load_extension("file_browser")
		end,
	},
	{
		"stevearc/aerial.nvim",
		config = function()
			require("aerial").setup({
				backends = { "treesitter", "lsp" },
			})
		end,
	},
	{
		"vim-scripts/camelcasemotion",
		lazy = false,
		config = function()
			vim.cmd([[
        map w <Plug>CamelCaseMotion_w
        map b <Plug>CamelCaseMotion_b
        map e <Plug>CamelCaseMotion_e
        sunmap w
        sunmap b
        sunmap e
      ]])
		end,
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		event = "BufReadPre",
		config = function()
			require("ibl").setup()
		end,
	},
	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		config = function()
			local npairs = require("nvim-autopairs")
			npairs.setup({
				check_ts = true,
			})
		end,
	},
	{
		"nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		config = function()
			local components = {
				-- {{{
				git_repo = {
					function()
						if
							#vim.api.nvim_list_tabpages() > 1
							and vim.fn.trim(vim.fn.system("git rev-parse --is-inside-work-tree")) == "true"
						then
							return vim.fn.trim(vim.fn.system("basename `git rev-parse --show-toplevel`"))
						end
					end,
				},
				separator = {
					function()
						return "%="
					end,
				},
				diff = {
					"diff",
					colored = false,
				},
				diagnostics = {
					"diagnostics",
					sources = { "nvim_diagnostic" },
					diagnostics_color = {
						error = "DiagnosticError",
						warn = "DiagnosticWarn",
						info = "DiagnosticInfo",
						hint = "DiagnosticHint",
					},
					colored = true,
				},
				lsp_client = {
					function(msg)
						local lsp_utils_ = require("base.lsp.utils")
						msg = msg or ""
						local buf_clients = vim.lsp.get_active_clients({ bufnr = 0 })

						if next(buf_clients) == nil then
							if type(msg) == "boolean" or #msg == 0 then
								return ""
							end
							return msg
						end

						local buf_ft = vim.bo.filetype
						local buf_client_names = {}

						for _, client in pairs(buf_clients) do
							if client.name ~= "null-ls" then
								table.insert(buf_client_names, client.name)
							end
						end

						vim.list_extend(buf_client_names, lsp_utils_.list_formatters(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils_.list_linters(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils_.list_hovers(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils_.list_code_actions(buf_ft))

						local hash = {}
						local client_names = {}
						for _, v in ipairs(buf_client_names) do
							if not hash[v] then
								client_names[#client_names + 1] = v
								hash[v] = true
							end
						end
						table.sort(client_names)
						return table.concat(client_names, ", ")
					end,
					colored = true,
					on_click = function()
						vim.cmd([[LspInfo]])
					end,
				},
				codeium = {
					function()
						return vim.fn["codeium#GetStatusString"]()
					end,
				},
			} -- }}}
			require("lualine").setup({
				options = {
					icons_enabled = true,
					theme = "auto",
					component_separators = { left = "", right = "" },
					section_separators = { left = "", right = "" },
					disabled_filetypes = {
						statusline = { "lazy" },
						winbar = { "help", "lazy" },
					},
					always_divide_middle = true,
					globalstatus = true,
				},
				sections = {
					lualine_a = { "mode" },
					lualine_b = { components.git_repo, "branch", components.diff, "diagnostics" },
					lualine_c = { "filename" },
					lualine_x = { components.codeium, "encoding", "fileformat", "filetype", "progress" },
					lualine_y = {},
					lualine_z = { "location" },
				},
				inactive_sections = {
					lualine_a = {},
					lualine_b = {},
					lualine_c = { "filename" },
					lualine_x = { "location" },
					lualine_y = {},
					lualine_z = {},
				},
				extensions = {},
			})
		end,
		require = { "nvim-web-devicons" },
	},
	{
		"chrisbra/unicode.vim",
		dependencies = {
			{ "junegunn/fzf", build = "./install --all" },
		},
		lazy = false,
		keys = {
			{ "<c-u>", "<plug>(UnicodeFuzzy)", { mode = "i", silent = true, noremap = true } },
		},
		config = function()
			vim.keymap.set("i", "<c-u>", "<plug>(UnicodeFuzzy)", { silent = true, noremap = true })
		end,
	},
	{
		"nvim-neo-tree/neo-tree.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim",
		},
		lazy = false,
		cmd = "Neotree",
		keys = {
			{ "<leader>f", "<cmd>Neotree toggle<cr>", { silent = true } },
		},
	},
	{ "christoomey/vim-sort-motion", lazy = false },
	{
		"numToStr/Navigator.nvim",
		lazy = false,
		config = function()
			require("Navigator").setup()
			vim.keymap.set("n", "<c-a>h", "<cmd>NavigatorLeft<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-a>j", "<cmd>NavigatorDown<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-a>k", "<cmd>NavigatorUp<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-a>l", "<cmd>NavigatorRight<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-s>", "<c-a>", keymap_opts)
		end,
	},
	{
		"Vonr/align.nvim",
		keys = {
			{ "aa", ":lua require('align').align_to_char(1, true)<cr>", mode = "x", { silent = true, noremap = true } },
			{
				"as",
				":lua require('align').align_to_char(2, true, true)<cr>",
				mode = "x",
				{ silent = true, noremap = true },
			},
			{
				"aw",
				":lua require('align').align_to_string(false, true, true)<cr>",
				mode = "x",
				{ silent = true, noremap = true },
			},
			{
				"ar",
				":lua require('align').align_to_string(true, true, true)<cr>",
				mode = "x",
				{ silent = true, noremap = true },
			},
			{
				"gaw",
				":lua require('align').operator(require('align').align_to_string, { is_pattern = false, reverse = true, preview = true })<cr>",
				mode = "n",
				{ silent = true, noremap = true },
			},
			{
				"gaa",
				":lua require('align').operator(require('align').align_to_char, { length = 1, reverse = true })<cr>",
				mode = "n",
				{ silent = true, noremap = true },
			},
		},
	},
	-- }}}

	-- coding {{{
	{
		"tpope/vim-surround",
		lazy = false,
	},
	{
		"numToStr/Comment.nvim",
		dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
		keys = { { "gc", mode = { "n", "v" } }, { "gcc", mode = { "n", "v" } }, { "gbc", mode = { "n", "v" } } },
		config = function(_, _)
			require("Comment").setup({
				ignore = "^$",
			})
		end,
	},
	{
		"andymass/vim-matchup",
		event = { "BufReadPost" },
		config = function()
			vim.g.matchup_matchparen_offscreen = { method = "popup" }
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"R-nvim/cmp-r",
		},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			local compare = require("cmp.config.compare")
			local source_names = {
				jupynium = "(Jupyter)",
				nvim_lsp = "(LSP)",
				luasnip = "(Snippet)",
				buffer = "(Buffer)",
				path = "(Path)",
			}
			local duplicates = {
				buffer = 1,
				path = 1,
				nvim_lsp = 0,
				luasnip = 1,
			}
			local has_words_before = function()
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0
					and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			cmp.setup({
				completion = {
					completeopt = "menu,menuone,noinsert,noselect",
				},
				preselect = cmp.PreselectMode.None,
				sorting = {
					priority_weight = 2,
					compare.score,
					compare.recently_used,
					compare.offset,
					compare.exact,
					compare.kind,
					compare.sort_text,
					compare.length,
					compare.order,
				},
				snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end,
				},
				mapping = cmp.mapping.preset.insert({
					["<c-space>"] = cmp.mapping.complete(),
					["<c-e>"] = cmp.mapping.abort(),
					["<cr>"] = cmp.mapping.confirm({ select = false }),
					["<tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						elseif has_words_before() then
							cmp.complete()
						else
							fallback()
						end
					end, { "i", "s" }),
					["<s-tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif luasnip.jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
					end, { "i", "s" }),
				}),
				sources = cmp.config.sources({
					{ name = "nvim_lsp_signature_help", group_index = 1 },
					{ name = "nvim_lsp", group_index = 1 },
					{ name = "luasnip", group_index = 1 },
					{ name = "cmp_r", group_index = 1 },
					{ name = "buffer", group_index = 2 },
					{ name = "path", group_index = 2 },
				}),
				formatting = {
					fields = { "kind", "abbr", "menu" },
					format = function(entry, item)
						local duplicates_default = 0
						item.menu = source_names[entry.source.name]
						item.dup = duplicates[entry.source.name] or duplicates_default
						return item
					end,
				},
			})
			require("cmp_r").setup()
		end,
	},
	{
		"L3MON4D3/LuaSnip",
		dependencies = {
			{
				"rafamadriz/friendly-snippets",
				config = function()
					require("luasnip.loaders.from_vscode").lazy_load()
				end,
			},
		},
		build = "make install_jsregexp",
		opts = {
			history = true,
			delete_check_events = "TextChanged",
		},
		config = function(_, opts)
			require("luasnip").setup(opts)
		end,
	},
	{
		"nvim-neotest/neotest",
		keys = {
			{
				"<leader>tn",
				"<cmd>lua require('neotest').run.run()<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>tf",
				"<cmd>lua require('neotest').run.run(vim.fn.expand('%'))<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>to",
				"<cmd>lua require('neotest').output_panel.toggle()<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>ts",
				"<cmd>lua require('neotest').summary.toggle()<cr>",
				mode = "n",
				silent = true,
			},
		},
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			"antoinemadec/FixCursorHold.nvim",
			"nvim-neotest/neotest-python",
			"nvim-neotest/nvim-nio",
		},
		opts = function()
			return {
				adapters = {},
				output = {
					enabled = false,
					open_on_run = false,
				},
				quickfix = {
					open = false,
				},
			}
		end,
		config = function(_, opts)
			require("neotest").setup(opts)
		end,
	},
	{
		"echasnovski/mini.hipatterns",
		event = "BufReadPre",
		opts = function()
			local hi = require("mini.hipatterns")
			return {
				highlighters = {
					hex_color = hi.gen_highlighter.hex_color({ priority = 2000 }),
				},
			}
		end,
	},
	{
		"echasnovski/mini.ai",
		event = "VeryLazy",
		dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" },
		opts = function()
			local ai = require("mini.ai")
			return {
				n_lines = 500,
				custom_textobjects = {
					o = ai.gen_spec.treesitter({
						a = { "@block.outer", "@conditional.outer", "@loop.outer" },
						i = { "@block.inner", "@conditional.inner", "@loop.inner" },
					}, {}),
					f = ai.gen_spec.treesitter({ a = "@function.outer", i = "@function.inner" }, {}),
					c = ai.gen_spec.treesitter({ a = "@class.outer", i = "@class.inner" }, {}),
				},
			}
		end,
		config = function(_, opts)
			require("mini.ai").setup(opts)
		end,
	},
	{
		"danymat/neogen",
		lazy = false,
		opts = {
			snippet_engine = "luasnip",
			enabled = true,
			languages = {},
		},
		keys = {
			{
				"<leader>dd",
				function()
					require("neogen").generate()
				end,
			},
			{
				"<leader>dc",
				function()
					require("neogen").generate({ type("class") })
				end,
			},
			{
				"<leader>df",
				function()
					require("neogen").generate({ type("func") })
				end,
			},
		},
		config = function(_, opts)
			require("neogen").setup(opts)
		end,
	},
	{
		"alvan/vim-closetag",
		ft = { "html" },
	},
	{
		"jpalardy/vim-slime",
		ft = { "matlab" },
		init = function()
			vim.g.slime_no_mappings = 1
			vim.g.slime_target = "tmux"
			vim.g.slime_default_config = { socket_name = "default", target_pane = "{down-of}" }
			vim.g.slime_dont_ask_default = 1
		end,
		keys = {
			{
				"<leader>s",
				"<plug>SlimeLineSend",
				mode = "n",
				silent = true,
			},
			{
				"<leader>s",
				"<plug>SlimeRegionSend",
				mode = "x",
				silent = true,
			},
		},
	},
	-- }}}

	-- treesitter {{{
	{
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
			"JoosepAlviste/nvim-ts-context-commentstring",
		},
		build = ":TSUpdate",
		event = { "BufReadPost", "BufNewFile" },
		opts = {
			sync_install = false,
			ensure_installed = {
				"bash",
				"dockerfile",
				"html",
				"json",
				"jsonc",
				"latex",
				"markdown",
				"markdown_inline",
				"python",
				"query",
				"regex",
				"vim",
				"vimdoc",
				"yaml",
			},
			highlight = { enable = true, additional_vim_regex_highlighting = {} },
			indent = { enable = true },
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "<c-space>",
					node_incremental = "<c-space>",
					scope_incremental = "<c-s>",
					node_decremental = "<m-space>",
				},
			},
			textobjects = {
				select = {
					enable = true,
					lookahead = true,
					keymaps = {
						["aa"] = "@parameter.outer",
						["ia"] = "@parameter.inner",
						["af"] = "@function.outer",
						["if"] = "@function.inner",
						["ac"] = "@class.outer",
						["ic"] = "@class.inner",
					},
				},
			},
			matchup = {
				enable = true,
			},
		},
		config = function(_, opts)
			if type(opts.ensure_installed) == "table" then
				local added = {}
				opts.ensure_installed = vim.tbl_filter(function(lang)
					if added[lang] then
						return false
					end
					added[lang] = true
					return true
				end, opts.ensure_installed)
			end
			require("nvim-treesitter.configs").setup(opts)
		end,
	},
	-- }}}

	-- lsp {{{
	{
		"neovim/nvim-lspconfig",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = {
			{ "j-hui/fidget.nvim", config = true, tag = "legacy" },
			"williamboman/mason.nvim",
			"williamboman/mason-lspconfig.nvim",
		},
		opts = {
			servers = {},
			setup = {},
			format = {
				timeout_ms = 3000,
			},
		},
		config = function(plugin, opts)
			-- format {{{

			local format = {}

			format.autoformat = true
			format.format_notify = false

			function format.toggle()
				format.autoformat = not format.autoformat
				vim.notify(format.autoformat and "Enabled format on save" or "Disabled format on save")
			end

			function format.format()
				local buf = vim.api.nvim_get_current_buf()

				local formatters = format.get_formatters(buf)
				local client_ids = vim.tbl_map(function(client)
					return client.id
				end, formatters.active)

				if #client_ids == 0 then
					return
				end

				if format.format_notify then
					format.notify(formatters)
				end

				vim.lsp.buf.format(vim.tbl_deep_extend("force", {
					bufnr = buf,
					filter = function(client)
						return vim.tbl_contains(client_ids, client.id)
					end,
				}, lsp_utils.opts("nvim-lspconfig").format or {}))
			end

			function format.notify(formatters)
				local lines = { "# Active:" }

				for _, client in ipairs(formatters.active) do
					local line = "- **" .. client.name .. "**"
					if client.name == "null-ls" then
						line = line
							.. " ("
							.. table.concat(
								vim.tbl_map(function(f)
									return "`" .. f.name .. "`"
								end, formatters.null_ls),
								", "
							)
							.. ")"
					end
					table.insert(lines, line)
				end

				if #formatters.available > 0 then
					table.insert(lines, "")
					table.insert(lines, "# Disabled:")
					for _, client in ipairs(formatters.available) do
						table.insert(lines, "- **" .. client.name .. "**")
					end
				end

				vim.notify(table.concat(lines, "\n"), vim.log.levels.INFO, {
					title = "Formatting",
					on_open = function(win)
						vim.api.nvim_win_set_option(win, "conceallevel", 3)
						vim.api.nvim_win_set_option(win, "spell", false)
						local buf = vim.api.nvim_win_get_buf(win)
						vim.treesitter.start(buf, "markdown")
					end,
				})
			end

			function format.supports_format(client)
				if
					client.config
					and client.config.capabilities
					and client.config.capabilities.documentFormattingProvider == false
				then
					return false
				end
				return client.supports_method("textDocument/formatting")
					or client.supports_method("textDocument/rangeFormatting")
			end

			function format.get_formatters(bufnr)
				local ft = vim.bo[bufnr].filetype
				-- check if we have any null-ls formatters for the current filetype
				local null_ls = package.loaded["null-ls"]
						and require("null-ls.sources").get_available(ft, "NULL_LS_FORMATTING")
					or {}

				local ret = {
					active = {},
					available = {},
					null_ls = null_ls,
				}

				local clients = vim.lsp.get_active_clients({ bufnr = bufnr })
				for _, client in ipairs(clients) do
					if format.supports_format(client) then
						if (#null_ls > 0 and client.name == "null-ls") or #null_ls == 0 then
							table.insert(ret.active, client)
						else
							table.insert(ret.available, client)
						end
					end
				end

				return ret
			end

			function format.on_attach(_, bufnr)
				vim.api.nvim_create_autocmd("BufWritePre", {
					group = vim.api.nvim_create_augroup("LspFormat." .. bufnr, {}),
					buffer = bufnr,
					callback = function()
						if format.autoformat then
							format.format()
						end
					end,
				})
			end

			-- }}}

			-- keymaps {{{

			local keymaps = {}

			function keymaps.on_attach(client, buffer)
				local self = keymaps.new(client, buffer)

				self:map("<c-h>", keymaps.diagnostic_goto(false))
				self:map("<c-l>", keymaps.diagnostic_goto(true))
				self:map("K", vim.lsp.buf.hover)
				self:map("gK", vim.lsp.buf.signature_help, { has = "signatureHelp" })
				self:map("gd", "Telescope lsp_definitions")
				self:map("gD", vim.lsp.buf.declaration)
				self:map("gr", "Telescope lsp_references")
				self:map("gI", "Telescope lsp_implementations")
				self:map("gb", "Telescope lsp_type_definitions")
				self:map("<leader>la", vim.lsp.buf.code_action, { mode = { "n", "v" }, has = "codeAction" })

				self:map("<leader>lf", format.format, { has = "documentFormatting" })
				self:map("<leader>lf", format.format, { mode = "v", has = "documentRangeFormatting" })
				self:map("<leader>lr", vim.lsp.buf.rename, { expr = true, has = "rename" })

				self:map("<leader>ls", require("telescope.builtin").lsp_document_symbols)
				self:map("<leader>lS", require("telescope.builtin").lsp_dynamic_workspace_symbols)
				self:map("<leader>lw", lsp_utils.toggle_diagnostics)
			end

			function keymaps.new(client, buffer)
				return setmetatable({ client = client, buffer = buffer }, { __index = keymaps })
			end

			function keymaps:has(cap)
				return self.client.server_capabilities[cap .. "Provider"]
			end

			function keymaps:map(lhs, rhs, opts_)
				opts_ = opts_ or {}
				if opts_.has and not self:has(opts_.has) then
					return
				end
				vim.keymap.set(
					opts_.mode or "n",
					lhs,
					type(rhs) == "string" and ("<cmd>%s<cr>"):format(rhs) or rhs,
					{ silent = true, buffer = self.buffer, expr = opts_.expr }
				)
			end

			function keymaps.diagnostic_goto(next, severity)
				local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
				severity = severity and vim.diagnostic[severity] or nil
				return function()
					go({ severity = severity })
				end
			end

			-- }}}

			-- servers {{{

			local servers = {}

			local function lsp_init()
				local config = {
					float = {
						focusable = true,
						style = "minimal",
						border = "rounded",
					},
					diagnostic = {
						virtual_text = {
							severity = {
								min = vim.diagnostic.severity.WARN,
							},
						},
						underline = true,
						update_in_insert = false,
						severity_sort = true,
						float = {
							focusable = true,
							style = "minimal",
							border = "rounded",
							source = "always",
							header = "",
							prefix = "",
						},
					},
				}

				vim.diagnostic.config(config.diagnostic)
			end

			function servers.setup(_, opts_)
				lsp_utils.on_attach(function(client, bufnr)
					format.on_attach(client, bufnr)
					keymaps.on_attach(client, bufnr)
				end)

				lsp_init()

				local servers_ = opts_.servers
				local capabilities = lsp_utils.capabilities()

				local function setup(server)
					local server_opts = vim.tbl_deep_extend("force", {
						capabilities = capabilities,
					}, servers_[server] or {})

					if opts_.setup[server] then
						if opts_.setup[server](server, server_opts) then
							return
						end
					elseif opts_.setup["*"] then
						if opts_.setup["*"](server, server_opts) then
							return
						end
					end
					require("lspconfig")[server].setup(server_opts)
				end

				local have_mason, mlsp = pcall(require, "mason-lspconfig")
				local all_mlsp_servers = {}
				if have_mason then
					all_mlsp_servers = vim.tbl_keys(require("mason-lspconfig.mappings.server").lspconfig_to_package)
				end

				local ensure_installed = {}
				for server, server_opts in pairs(servers_) do
					if server_opts then
						server_opts = server_opts == true and {} or server_opts
						if server_opts.mason == false or not vim.tbl_contains(all_mlsp_servers, server) then
							setup(server)
						else
							ensure_installed[#ensure_installed + 1] = server
						end
					end
				end

				if have_mason then
					mlsp.setup({ ensure_installed = ensure_installed })
					mlsp.setup_handlers({ setup })
				end
			end

			-- }}}

			servers.setup(plugin, opts)
		end,
	},
	{
		"williamboman/mason.nvim",
		build = ":MasonUpdate",
		cmd = "Mason",
		opts = {
			ensure_installed = {
				"shfmt",
			},
		},
		config = function(_, opts)
			require("mason").setup(opts)
			local mr = require("mason-registry")
			local function ensure_installed()
				for _, tool in ipairs(opts.ensure_installed) do
					local p = mr.get_package(tool)
					if not p:is_installed() then
						p:install()
					end
				end
			end
			if mr.refresh then
				mr.refresh(ensure_installed)
			else
				ensure_installed()
			end
		end,
	},
	{
		"nvimtools/none-ls.nvim",
		event = "BufReadPre",
		dependencies = { "mason.nvim" },
		opts = function()
			local nls = require("null-ls")
			return {
				root_dir = require("null-ls.utils").root_pattern(
					".null-ls-root",
					".neoconf.json",
					"Makefile",
					"makefile",
					".git"
				),
				sources = {
					nls.builtins.formatting.shfmt,
				},
			}
		end,
	},
	-- }}}
}

-- }}}

-- languages {{{
-- csv {{{
vim.list_extend(plugins, {
	{
		"chrisbra/csv.vim",
		ft = "csv",
		config = function()
			vim.g.csv_default_delim = ","
		end,
	},
})
-- }}}

-- docker {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			if type(opts.ensure_installed) == "table" then
				vim.list_extend(opts.ensure_installed, { "dockerfile" })
			end
		end,
	},
	{
		"nvimtools/none-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")
			opts.sources = opts.sources or {}
			vim.list_extend(opts.sources, {
				nls.builtins.diagnostics.hadolint,
			})
		end,
		dependencies = {
			"mason.nvim",
			opts = function(_, opts)
				opts.ensure_installed = opts.ensure_installed or {}
				vim.list_extend(opts.ensure_installed, { "hadolint" })
			end,
		},
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				dockerls = {},
				docker_compose_language_service = {},
			},
		},
	},
})
-- }}}

-- html {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "html", "css" })
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "prettierd", "stylelint" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			-- make sure mason installs the server
			servers = {
				-- html
				html = {
					filetypes = {
						"html",
						"javascript",
						"javascriptreact",
						"javascript.jsx",
						"typescript",
						"typescriptreact",
						"typescript.tsx",
					},
				},
				-- Emmet
				emmet_ls = {
					init_options = {
						html = {
							options = {
								-- For possible options, see: https://github.com/emmetio/emmet/blob/master/src/config.ts#L79-L267
								["bem.enabled"] = true,
							},
						},
					},
				},
				-- CSS
				cssls = {},
			},
		},
	},
	{
		"nvimtools/none-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")
			table.insert(opts.sources, nls.builtins.formatting.prettierd)
			table.insert(opts.sources, nls.builtins.formatting.stylelint)
		end,
	},
	{
		"uga-rosa/ccc.nvim",
		opts = {},
		cmd = { "CccPick", "CccConvert", "CccHighlighterEnable", "CccHighlighterDisable", "CccHighlighterToggle" },
		keys = {
			{ "<leader>zCp", "<cmd>CccPick<cr>", desc = "Pick" },
			{ "<leader>zCc", "<cmd>CccConvert<cr>", desc = "Convert" },
			{ "<leader>zCh", "<cmd>CccHighlighterToggle<cr>", desc = "Toggle Highlighter" },
		},
	},
})
-- }}}

-- javascript {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "javascript" })
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "prettierd", "typescript-language-server" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ts_ls = {},
			},
		},
	},
	{
		"nvimtools/none-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")
			table.insert(opts.sources, nls.builtins.formatting.prettierd)
		end,
	},
})
-- }}}

-- json {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			if type(opts.ensure_installed) == "table" then
				vim.list_extend(opts.ensure_installed, { "json", "json5", "jsonc" })
			end
		end,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"b0o/SchemaStore.nvim",
		},
		opts = {
			servers = {
				jsonls = {
					on_new_config = function(new_config)
						new_config.settings.json.schemas = new_config.settings.json.schemas or {}
						vim.list_extend(new_config.settings.json.schemas, require("schemastore").json.schemas())
					end,
					settings = {
						json = {
							format = {
								enable = true,
							},
							validate = { enable = true },
						},
					},
				},
			},
		},
	},
})
-- }}}

-- lua {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "lua", "luadoc", "luap" })
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "stylua" })
		end,
	},
	{
		"nvimtools/none-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")
			table.insert(opts.sources, nls.builtins.formatting.stylua)
		end,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{
				"folke/neodev.nvim",
				opts = {
					library = { plugins = { "neotest" }, types = true },
				},
			},
		},
		opts = {
			servers = {
				lua_ls = {
					settings = {
						Lua = {
							workspace = {
								checkThirdParty = false,
							},
							completion = { callSnippet = "Replace" },
							telemetry = { enable = false },
							hint = {
								enable = false,
							},
							diagnostics = {
								globals = { "vim" },
							},
						},
					},
				},
			},
		},
	},
})
-- }}}

-- markdown {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "markdown" })
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "ltex-ls" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"barreiroleo/ltex_extra.nvim",
		},
		opts = {
			servers = {
				ltex = {
					on_attach = function(_, _)
						require("ltex_extra").setup({
							load_langs = { "en-CA", "fr" },
							init_check = true,
							path = ".ltex",
							log_level = "debug",
						})
					end,
					settings = {
						ltex = {
							language = "auto",
						},
					},
				},
			},
		},
	},
	{
		"iamcco/markdown-preview.nvim",
		build = "cd app && yarn install",
		init = function()
			vim.g.mkdp_filetypes = { "markdown" }
		end,
		ft = "markdown",
		cmd = { "MarkdownPreview" },
	},
	{ "plasticboy/vim-markdown", ft = "markdown" },
})
-- }}}

-- python {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "python", "toml" })
			vim.treesitter.query.set(
				"python",
				"folds",
				[[
        [
          (function_definition)
          (class_definition)
          (string)
        ] @fold
      ]]
			)
		end,
	},
	{
		"nvimtools/none-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")

			table.insert(opts.sources, nls.builtins.formatting.isort)
			table.insert(
				opts.sources,
				nls.builtins.formatting.black.with({
					args = { "--stdin-filename", "$FILENAME", "--quiet", "--line-length", "79", "-" },
				})
			)
			-- table.insert(
			-- 	opts.sources,
			-- 	nls.builtins.diagnostics.mypy.with({
			-- 		extra_args = { "--ignore-missing-imports" },
			-- 	})
			-- )
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "debugpy", "black", "isort", "mypy" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ruff = {},
				basedpyright = {
					settings = {
						basedpyright = {
							analysis = {
								autoImportCompletions = true,
								typeCheckingMode = "basic",
								autoSearchPaths = true,
								useLibraryCodeForTypes = true,
								diagnosticMode = "openFilesOnly",
							},
						},
					},
				},
				-- pylyzer = {},
			},
			setup = {
				ruff = function()
					lsp_utils.on_attach(function(client, _)
						if client.name == "ruff" then
							client.server_capabilities.hoverProvider = false
						end
					end)
				end,
			},
		},
	},
	{
		"nvim-neotest/neotest",
		dependencies = {
			"nvim-neotest/neotest-python",
		},
		opts = function(_, opts)
			vim.list_extend(opts.adapters, {
				require("neotest-python")({
					runner = "pytest",
				}),
			})
		end,
	},
	{
		"danymat/neogen",
		opts = function(_, opts)
			opts.languages["python"] = {
				template = {
					annotation_convention = "numpydoc",
				},
			}
		end,
	},
	{
		"microsoft/python-type-stubs",
		cond = false,
	},
	{
		"kalekundert/vim-coiled-snake",
		ft = "python",
	},
})
-- }}}

-- latex {{{
local ignored_latex_errors = {
	"Underfull",
	"Overfull",
	"Package balance",
	"Unused global",
	"Package fancyhdr Warning",
	"There's no line here to end",
	"No hyphenation patterns",
	"include should only be used after",
}
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "latex" })
			vim.list_extend(opts.highlight.additional_vim_regex_highlighting, { "latex" })
		end,
	},
	-- {
	-- 	"nvimtools/none-ls.nvim",
	-- 	opts = function(_, opts)
	-- 		local nls = require("null-ls")

	-- 		table.insert(opts.sources, nls.builtins.formatting.latexindent)
	-- 	end,
	-- },
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "texlab", "ltex-ls" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"barreiroleo/ltex_extra.nvim",
		},
		opts = {
			servers = {
				texlab = {
					settings = {
						texlab = {
							diagnostics = {
								ignoredPatterns = ignored_latex_errors,
							},
						},
					},
				},
				ltex = {
					on_attach = function(_, _)
						require("ltex_extra").setup({
							load_langs = { "en-CA", "fr" },
							init_check = true,
							path = ".ltex",
							log_level = "debug",
						})
					end,
					settings = {
						ltex = {
							enabled = { "latex" },
							language = "auto",
						},
					},
				},
			},
		},
	},
	{
		"lervag/vimtex",
		ft = "tex",
		config = function()
			vim.g.tex_flavor = "latex"
			vim.g.vimtex_compiler_method = "latexmk"
			vim.g.vimtex_fold_enabled = true
			vim.g.vimtex_format_enabled = true
			vim.g.vimtex_view_enabled = true
			vim.g.vimtex_view_automatic = true
			vim.g.vimtex_view_method = "zathura"
			vim.g.vimtex_view_forward_search_on_start = false
			vim.g.vimtex_compiler_latexmk = {
				build_dir = "",
				callback = true,
				continuous = true,
				executable = "latexmk",
				hooks = {},
				options = {
					"--verbose",
					"--file-line-error",
					"--synctex=1",
					"--interaction=nonstopmode",
					"--shell-escape",
				},
			}
			vim.g.vimtex_quickfix_ignore_filters = ignored_latex_errors
		end,
	},
})

-- keymaps {{{
vim.keymap.set("n", "<leader>tex", ":-1read $HOME/.config/nvim/templates/skeleton.tex<cr>", keymap_opts)
-- }}}

-- }}}

-- r {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "r", "markdown", "rnoweb", "yaml" })
		end,
	},
	{
		"R-nvim/R.nvim",
		ft = "r",
		config = function()
			local opts = {
				R_args = { "--quiet", "--no-save" },
				external_term = true,
			}
			if vim.env.R_AUTO_START == "true" then
				opts.auto_start = "on startup"
				opts.objbr_auto_start = true
			end
			require("r").setup(opts)
		end,
	},
})
-- }}}

-- matlab {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "matlab" })
		end,
	},
})
-- }}}

-- c {{{
vim.list_extend(plugins, {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "c" })
		end,
	},
})
-- }}}

-- }}}

-- {{{ plugin installation
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup(plugins, {
	defaults = { lazy = true, version = nil },
	install = { missing = true, colorscheme = { "nord" } },
	checker = { enabled = false },
	performance = {
		cache = {
			enabled = true,
		},
		rtp = {
			disabled_plugins = {
				"gzip",
				"matchit",
				"matchparen",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
})
-- }}}
