font xft:Dank Mono 10

set $up k
set $down j
set $left h
set $right l
set $mod Mod4

floating_modifier Mod4

bindsym Mod4+Return exec alacritty
# bindsym Mod4+Return exec termite
bindsym Mod4+t exec alacritty

bindsym Mod4+Shift+q kill

bindsym Mod4+Shift+e exec i3-msg exit

bindsym Mod4+p exec dmenu_run

# bindsym Mod4+Shift+p exec /usr/bin/flameshot gui -r | xclip -selection clipboard -t image/png
bindsym Mod4+Shift+p exec /usr/bin/flameshot gui

# spotify
# bindsym Control+Right exec playerctl --player spotify next
# bindsym Control+Right exec if [ $(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2) = 2 ]; then playerctl --player spotify next; fi 
# bindsym Control+Left exec playerctl --player spotify previous
# bindsym Control+Up exec playerctl --player spotify volume 0.05+
# bindsym Control+Down exec playerctl --player spotify volume 0.05-
# bindsym Control+space exec playerctl --player spotify play-pause


# change focus
bindsym Mod4+$left focus left
bindsym Mod4+$down focus down
bindsym Mod4+$up focus up
bindsym Mod4+$right focus right

# move focused window
bindsym Mod4+Shift+$left move left
bindsym Mod4+Shift+$down move down
bindsym Mod4+Shift+$up move up
bindsym Mod4+Shift+$right move right

# move workspace to monitor
bindsym Mod4+Shift+Left move workspace to output left
bindsym Mod4+Shift+Down move workspace to output down
bindsym Mod4+Shift+Up move workspace to output up
bindsym Mod4+Shift+Right move workspace to output right

# split in horizontal orientation
bindsym Mod4+b split h

# split in vertical orientation
bindsym Mod4+v split v

# enter fullscreen mode for the focused container
bindsym Mod4+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# bindsym Mod4+s layout stacking
# bindsym Mod4+w layout tabbed
# bindsym Mod4+e layout toggle split

# toggle tiling / floating
bindsym Mod4+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym Mod4+space focus mode_toggle

# backlight
bindsym Mod4+Up exec xbacklight +10
bindsym Mod4+Down exec xbacklight -10

# notifications
bindsym Mod1+Escape exec dunstctl close


# switch to workspace
bindsym Mod4+1 workspace 1
bindsym Mod4+2 workspace 2
bindsym Mod4+3 workspace 3
bindsym Mod4+4 workspace 4
bindsym Mod4+5 workspace 5
bindsym Mod4+6 workspace 6
bindsym Mod4+7 workspace 7
bindsym Mod4+8 workspace 8
bindsym Mod4+9 workspace 9
bindsym Mod4+0 workspace 10

# move focused container to workspace
bindsym Mod4+Shift+1 move container to workspace 1
bindsym Mod4+Shift+2 move container to workspace 2
bindsym Mod4+Shift+3 move container to workspace 3
bindsym Mod4+Shift+4 move container to workspace 4
bindsym Mod4+Shift+5 move container to workspace 5
bindsym Mod4+Shift+6 move container to workspace 6
bindsym Mod4+Shift+7 move container to workspace 7
bindsym Mod4+Shift+8 move container to workspace 8
bindsym Mod4+Shift+9 move container to workspace 9
bindsym Mod4+Shift+0 move container to workspace 10

# reload the configuration file
bindsym Mod4+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym Mod4+Shift+r restart

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym $left       resize shrink width 1 px or 1 ppt
        bindsym $down       resize grow height 1 px or 1 ppt
        bindsym $up         resize shrink height 1 px or 1 ppt
        bindsym $right      resize grow width 1 px or 1 ppt

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym Mod4+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        i3bar_command i3bar -t
        status_command i3blocks -c ~/.config/i3/i3blocks.conf
        position top
        colors {
          background #1b2b3400
        }
}

# start composite manager
exec --no-startup-id xcompmgr

# makes the mouse invisible after some time
exec --no-startup-id unclutter

# set background
exec --no-startup-id xwallpaper --zoom ~/.config/wallpaper.png

# screen lock
# bindsym Control+Mod4+l exec ~/.config/i3/lock.sh
bindsym Control+Mod4+l exec alacritty -e physlock

# audio control
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle

# brightness control
bindsym XF86MonBrightnessUp exec echo $(($(cat /sys/class/backlight/intel_backlight/brightness) + 20)) | tee /sys/class/backlight/intel_backlight/brightness
bindsym XF86MonBrightnessDown exec echo $(($(cat /sys/class/backlight/intel_backlight/brightness) - 20)) | tee /sys/class/backlight/intel_backlight/brightness

# shortcuts
bindsym Control+Mod1+b exec --no-startup-id firefox
bindsym Control+Mod1+p exec --no-startup-id evince
bindsym Control+Mod1+Delete exec --no-startup-id shutdown -h now
bindsym Control+Mod4+b exec --no-startup-id ~/documents/code/dmenu/bib
bindsym Control+Mod1+o exec --no-startup-id obsidian

# no border for new windows
new_window none
default_border pixel 1

# gaps
gaps inner 5
# only enable gaps on a workspace when there is at least one container
smart_gaps on
# activate smart borders (always)
smart_borders on

# custom notifications placement
no_focus [title="Microsoft Teams Notification"]
for_window [title="Microsoft Teams Notification"] move absolute position 4812 px 1316 px
