return {
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			{ "nvim-telescope/telescope-file-browser.nvim" },
			{ "stevearc/aerial.nvim" },
		},
		cmd = "Telescope",
		keys = {
			{ "<c-f>", "<cmd>Telescope find_files<cr>" },
			{ "<c-g>", "<cmd>Telescope git_files<cr>" },
			{ "<c-p>", "<cmd>Telescope live_grep<cr>" },
			{ "<c-t>", "<cmd>Telescope aerial<cr>" },
			{
				"<c-b>",
				"<cmd>lua require('telescope.builtin').buffers({ sort_lastused = true, ignore_current_buffer = true})<cr>",
				desc = "Buffers",
			},
			{ "<c-d>", "<cmd>Telescope file_browser grouped=true<cr>", desc = "Find files (grouped)" },
		},
		opts = {
			extensions = {
				file_browser = {
					hijack_netrw = true,
				},
			},
			defaults = {
				file_ignore_patterns = {
					"node_modules",
					"*.app",
					"*.aux",
					"*.bbl",
					"*.bcf",
					"*.blg",
					"*.dvi",
					"*.fdb_latexmk",
					"*.fls",
					"*.glo",
					"*.ist",
					"*.lof",
					"*.log",
					"*.lot",
					"*.nlo",
					"*.out",
					"*.run.xml",
					"*.sta",
					"*.synctex*",
					"*.synctex.gz",
					"*.tdo",
					"*.toc",
					"*.xcp",
				},
				mappings = {
					i = {
						["<C-j>"] = function(...)
							require("telescope.actions").move_selection_next(...)
						end,
						["<C-k>"] = function(...)
							require("telescope.actions").move_selection_previous(...)
						end,
						["<C-n>"] = function(...)
							require("telescope.actions").cycle_history_next(...)
						end,
						["<C-p>"] = function(...)
							require("telescope.actions").cycle_history_prev(...)
						end,
					},
				},
			},
		},
		config = function(_, opts)
			local telescope = require("telescope")
			telescope.setup(opts)
			telescope.load_extension("aerial")
			telescope.load_extension("file_browser")
		end,
	},
	{
		"stevearc/aerial.nvim",
		config = function()
			require("aerial").setup({
				backends = { "treesitter", "lsp" },
			})
		end,
	},
	{
		"vim-scripts/camelcasemotion",
		lazy = false,
		config = function()
			vim.cmd([[
        map w <Plug>CamelCaseMotion_w
        map b <Plug>CamelCaseMotion_b
        map e <Plug>CamelCaseMotion_e
        sunmap w
        sunmap b
        sunmap e
      ]])
		end,
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		event = "BufReadPre",
		config = true,
	},
	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		config = function()
			local npairs = require("nvim-autopairs")
			npairs.setup({
				check_ts = true,
			})
		end,
	},
	{
		"nvim-lualine/lualine.nvim",
		event = "VeryLazy",
		config = function()
			local components = {
				-- {{{
				git_repo = {
					function()
						if
							#vim.api.nvim_list_tabpages() > 1
							and vim.fn.trim(vim.fn.system("git rev-parse --is-inside-work-tree")) == "true"
						then
							return vim.fn.trim(vim.fn.system("basename `git rev-parse --show-toplevel`"))
						end
					end,
				},
				separator = {
					function()
						return "%="
					end,
				},
				diff = {
					"diff",
					colored = false,
				},
				diagnostics = {
					"diagnostics",
					sources = { "nvim_diagnostic" },
					diagnostics_color = {
						error = "DiagnosticError",
						warn = "DiagnosticWarn",
						info = "DiagnosticInfo",
						hint = "DiagnosticHint",
					},
					colored = true,
				},
				lsp_client = {
					function(msg)
						local lsp_utils = require("base.lsp.utils")
						msg = msg or ""
						local buf_clients = vim.lsp.get_active_clients({ bufnr = 0 })

						if next(buf_clients) == nil then
							if type(msg) == "boolean" or #msg == 0 then
								return ""
							end
							return msg
						end

						local buf_ft = vim.bo.filetype
						local buf_client_names = {}

						for _, client in pairs(buf_clients) do
							if client.name ~= "null-ls" then
								table.insert(buf_client_names, client.name)
							end
						end

						vim.list_extend(buf_client_names, lsp_utils.list_formatters(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils.list_linters(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils.list_hovers(buf_ft))
						vim.list_extend(buf_client_names, lsp_utils.list_code_actions(buf_ft))

						local hash = {}
						local client_names = {}
						for _, v in ipairs(buf_client_names) do
							if not hash[v] then
								client_names[#client_names + 1] = v
								hash[v] = true
							end
						end
						table.sort(client_names)
						return table.concat(client_names, ", ")
					end,
					colored = true,
					on_click = function()
						vim.cmd([[LspInfo]])
					end,
				},
				codeium = {
					function()
						return vim.fn["codeium#GetStatusString"]()
					end,
				},
			} -- }}}
			require("lualine").setup({
				options = {
					icons_enabled = true,
					theme = "auto",
					component_separators = { left = "", right = "" },
					section_separators = { left = "", right = "" },
					disabled_filetypes = {
						statusline = { "lazy" },
						winbar = { "help", "lazy" },
					},
					always_divide_middle = true,
					globalstatus = true,
				},
				sections = {
					lualine_a = { "mode" },
					lualine_b = { components.git_repo, "branch", components.diff, "diagnostics" },
					lualine_c = { "filename" },
					lualine_x = { components.codeium, "encoding", "fileformat", "filetype", "progress" },
					lualine_y = {},
					lualine_z = { "location" },
				},
				inactive_sections = {
					lualine_a = {},
					lualine_b = {},
					lualine_c = { "filename" },
					lualine_x = { "location" },
					lualine_y = {},
					lualine_z = {},
				},
				extensions = {},
			})
		end,
		require = { "nvim-web-devicons" },
	},
	{
		"chrisbra/unicode.vim",
		dependencies = {
			{ "junegunn/fzf", build = "./install --all" },
		},
		lazy = false,
		keys = {
			{ "<c-u>", "<plug>(UnicodeFuzzy)", { mode = "i", silent = true, noremap = true } },
		},
		config = function()
			vim.keymap.set("i", "<c-u>", "<plug>(UnicodeFuzzy)", { silent = true, noremap = true })
		end,
	},
	{
		"nvim-neo-tree/neo-tree.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim",
		},
		cmd = "Neotree",
		keys = {
			{ "<leader>f", "<cmd>Neotree toggle<cr>", { silent = true } },
		},
	},
	{ "christoomey/vim-sort-motion", lazy = false },
}
