return {
	{ "tpope/vim-repeat", event = "VeryLazy" },
	{ "nvim-lua/plenary.nvim", event = "VeryLazy" },
	{
		"TimUntersberger/neogit",
		cmd = "Neogit",
		keys = {
			{ "<leader>gs", "<cmd>Neogit kind=floating<cr>", desc = "Status" },
		},
		config = function()
			require("neogit").setup({
				disable_commit_confirmation = false,
				popup = {
					kind = "replace",
				},
			})
		end,
	},
	{
		"lewis6991/gitsigns.nvim",
		lazy = false,
		config = function()
			require("gitsigns").setup()
		end,
	},
	{
		"numToStr/Navigator.nvim",
		lazy = false,
		config = function()
			require("Navigator").setup()
			vim.keymap.set("n", "<c-a>h", "<cmd>NavigatorLeft<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-a>j", "<cmd>NavigatorDown<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-a>k", "<cmd>NavigatorUp<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-a>l", "<cmd>NavigatorRight<cr>", { silent = true, noremap = true })
			vim.keymap.set("n", "<c-s>", "<c-a>", opts)
		end,
	},
	{
		"Vonr/align.nvim",
		keys = {
			{ "aa", ":lua require('align').align_to_char(1, true)<cr>", mode = "x", { silent = true, noremap = true } },
			{ "as", ":lua require('align').align_to_char(2, true, true)<cr>", mode = "x", { silent = true, noremap = true } },
			{
				"aw",
				":lua require('align').align_to_string(false, true, true)<cr>",
				mode = "x",
				{ silent = true, noremap = true },
			},
			{
				"ar",
				":lua require('align').align_to_string(true, true, true)<cr>",
				mode = "x",
				{ silent = true, noremap = true },
			},
			{
				"gaw",
				":lua require('align').operator(require('align').align_to_string, { is_pattern = false, reverse = true, preview = true })<cr>",
				mode = "n",
				{ silent = true, noremap = true },
			},
			{
				"gaa",
				":lua require('align').operator(require('align').align_to_char, { length = 1, reverse = true })<cr>",
				mode = "n",
				{ silent = true, noremap = true },
			},
		},
	},
}
