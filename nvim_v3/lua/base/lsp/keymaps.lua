local M = {}

function M.on_attach(client, buffer)
	local self = M.new(client, buffer)

	self:map("<c-h>", M.diagnostic_goto(false))
	self:map("<c-l>", M.diagnostic_goto(true))
	self:map("K", vim.lsp.buf.hover)
	self:map("gK", vim.lsp.buf.signature_help, { has = "signatureHelp" })
	self:map("gd", "Telescope lsp_definitions")
	self:map("gD", vim.lsp.buf.declaration)
	self:map("gr", "Telescope lsp_references")
	self:map("gI", "Telescope lsp_implementations")
	self:map("gb", "Telescope lsp_type_definitions")
	self:map("<leader>la", vim.lsp.buf.code_action, { mode = { "n", "v" }, has = "codeAction" })

	local format = require("base.lsp.format").format
	self:map("<leader>lf", format, { has = "documentFormatting" })
	self:map("<leader>lf", format, { mode = "v", has = "documentRangeFormatting" })
	self:map("<leader>lr", vim.lsp.buf.rename, { expr = true, has = "rename" })

	self:map("<leader>ls", require("telescope.builtin").lsp_document_symbols)
	self:map("<leader>lS", require("telescope.builtin").lsp_dynamic_workspace_symbols)
	self:map("<leader>lw", require("base.lsp.utils").toggle_diagnostics)
end

function M.new(client, buffer)
	return setmetatable({ client = client, buffer = buffer }, { __index = M })
end

function M:has(cap)
	return self.client.server_capabilities[cap .. "Provider"]
end

function M:map(lhs, rhs, opts)
	opts = opts or {}
	if opts.has and not self:has(opts.has) then
		return
	end
	vim.keymap.set(
		opts.mode or "n",
		lhs,
		type(rhs) == "string" and ("<cmd>%s<cr>"):format(rhs) or rhs,
		{ silent = true, buffer = self.buffer, expr = opts.expr }
	)
end

function M.diagnostic_goto(next, severity)
	local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
	severity = severity and vim.diagnostic[severity] or nil
	return function()
		go({ severity = severity })
	end
end

return M
