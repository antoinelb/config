return {
	{
		"tpope/vim-surround",
		lazy = false,
	},
	{
		"numToStr/Comment.nvim",
		dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
		keys = { { "gc", mode = { "n", "v" } }, { "gcc", mode = { "n", "v" } }, { "gbc", mode = { "n", "v" } } },
		config = function(_, _)
			require("Comment").setup({
				ignore = "^$",
				pre_hook = require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook(),
			})
		end,
	},
	{
		"andymass/vim-matchup",
		event = { "BufReadPost" },
		config = function()
			vim.g.matchup_matchparen_offscreen = { method = "popup" }
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
		},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			local neogen = require("neogen")
			local compare = require("cmp.config.compare")
			local source_names = {
				nvim_lsp = "(LSP)",
				luasnip = "(Snippet)",
				buffer = "(Buffer)",
				path = "(Path)",
			}
			local duplicates = {
				buffer = 1,
				path = 1,
				nvim_lsp = 0,
				luasnip = 1,
			}
			local has_words_before = function()
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0
					and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			cmp.setup({
				completion = {
					completeopt = "menu,menuone,noinsert,noselect",
				},
				preselect = cmp.PreselectMode.None,
				sorting = {
					priority_weight = 2,
					compare.score,
					compare.recently_used,
					compare.offset,
					compare.exact,
					compare.kind,
					compare.sort_text,
					compare.length,
					compare.order,
				},
				snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end,
				},
				mapping = cmp.mapping.preset.insert({
					["<c-space>"] = cmp.mapping.complete(),
					["<c-e>"] = cmp.mapping.abort(),
					["<cr>"] = cmp.mapping.confirm({ select = false }),
					["<tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif neogen.jumpable() then
							neogen.jump_next()
						elseif luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						elseif has_words_before() then
							cmp.complete()
						else
							fallback()
						end
					end, { "i", "s" }),
					["<s-tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif neogen.jumpable(true) then
							neogen.jump_prev()
						elseif luasnip.jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
					end, { "i", "s" }),
				}),
				sources = cmp.config.sources({
					{ name = "nvim_lsp_signature_help", group_index = 1 },
					{ name = "nvim_lsp", group_index = 1 },
					{ name = "luasnip", group_index = 1 },
					{ name = "buffer", group_index = 2 },
					{ name = "path", group_index = 2 },
				}),
				formatting = {
					fields = { "kind", "abbr", "menu" },
					format = function(entry, item)
						local duplicates_default = 0
						item.menu = source_names[entry.source.name]
						item.dup = duplicates[entry.source.name] or duplicates_default
						return item
					end,
				},
			})
		end,
	},
	{
		"L3MON4D3/LuaSnip",
		dependencies = {
			{
				"rafamadriz/friendly-snippets",
				config = function()
					require("luasnip.loaders.from_vscode").lazy_load()
				end,
			},
		},
		build = "make install_jsregexp",
		opts = {
			history = true,
			delete_check_events = "TextChanged",
		},
		-- keys = {
		-- 	{
		-- 		"<tab>",
		-- 		function()
		-- 			return require("luasnip").jumpable(1) and "<plug>luasnip-jump-next" or "<tab>"
		-- 		end,
		-- 		expr = true,
		-- 		remap = true,
		-- 		silent = true,
		-- 		mode = "i",
		-- 	},
		-- 	{
		-- 		"<tab>",
		-- 		function()
		-- 			require("luasnip").jump(1)
		-- 		end,
		-- 		mode = "s",
		-- 	},
		-- 	{
		-- 		"<s-tab>",
		-- 		function()
		-- 			require("luasnip").jump(-1)
		-- 		end,
		-- 		mode = { "i", "s" },
		-- 	},
		-- },
		config = function(_, opts)
			require("luasnip").setup(opts)
		end,
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		event = "BufReadPre",
		config = true,
	},
	{
		"nvim-neotest/neotest",
		keys = {
			{
				"<leader>tn",
				"<cmd>lua require('neotest').run.run()<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>tf",
				"<cmd>lua require('neotest').run.run(vim.fn.expand('%'))<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>to",
				"<cmd>lua require('neotest').output_panel.toggle()<cr>",
				mode = "n",
				silent = true,
			},
			{
				"<leader>ts",
				"<cmd>lua require('neotest').summary.toggle()<cr>",
				mode = "n",
				silent = true,
			},
		},
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			"antoinemadec/FixCursorHold.nvim",
			"nvim-neotest/neotest-python",
		},
		opts = function()
			return {
				adapters = {},
				output = {
					enabled = false,
					open_on_run = false,
				},
				quickfix = {
					open = false,
				},
			}
		end,
		config = function(_, opts)
			require("neotest").setup(opts)
		end,
	},
	{
		"echasnovski/mini.hipatterns",
		event = "BufReadPre",
		opts = function()
			local hi = require("mini.hipatterns")
			return {
				highlighters = {
					hex_color = hi.gen_highlighter.hex_color({ priority = 2000 }),
				},
			}
		end,
	},
	{
		"echasnovski/mini.ai",
		event = "VeryLazy",
		dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" },
		opts = function()
			local ai = require("mini.ai")
			return {
				n_lines = 500,
				custom_textobjects = {
					o = ai.gen_spec.treesitter({
						a = { "@block.outer", "@conditional.outer", "@loop.outer" },
						i = { "@block.inner", "@conditional.inner", "@loop.inner" },
					}, {}),
					f = ai.gen_spec.treesitter({ a = "@function.outer", i = "@function.inner" }, {}),
					c = ai.gen_spec.treesitter({ a = "@class.outer", i = "@class.inner" }, {}),
				},
			}
		end,
		config = function(_, opts)
			require("mini.ai").setup(opts)
		end,
	},
	{
		"danymat/neogen",
		lazy = false,
		opts = {
			snippet_engine = "luasnip",
			enabled = true,
			languages = {},
		},
		keys = {
			{
				"<leader>dd",
				function()
					require("neogen").generate()
				end,
			},
			{
				"<leader>dc",
				function()
					require("neogen").generate({ type("class") })
				end,
			},
			{
				"<leader>df",
				function()
					require("neogen").generate({ type("func") })
				end,
			},
		},
		config = function(_, opts)
			require("neogen").setup(opts)
		end,
	},
}
