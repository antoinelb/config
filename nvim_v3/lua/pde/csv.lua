return {
  {
    "chrisbra/csv.vim",
    ft = "csv",
    config = function()
      vim.g.csv_default_delim = ","
    end,
  },
}
